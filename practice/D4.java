//练习判断某年某月的天数
import java.util.Scanner;
public class D4 {
	public static void main(String[] args) {
		System.out.println("请按照格式输入年份和月份：年份 月份");
		Scanner sc=new Scanner(System.in);
		int year=sc.nextInt();
		int month=sc.nextInt();
		if(year<0|month<0|month>12)
			System.out.println("输入有误！");
		int days=0;
		switch (month){
			case 1:
		    case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: 
				days=31;
			break;
			case 4:
			case 6:
			case 9:
			case 11:
				days=30;
			break;
			}
			if (month==2){
				if(((year%4==0)&(year%100!=0))|(year%400==0)){
					days=29;
				}else
					days=28;
			}
			System.out.println(year+"年"+month+"月"+"有"+days+"天");
	}
	
}
