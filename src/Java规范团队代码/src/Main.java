import java.math.BigInteger;
import java.security.Key;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        System.out.println("输入需要加密的文件名：");
        Scanner sc=new Scanner(System.in);
        String name=sc.nextLine();
        try {
            //使用混合密码体制DES-RSA加密文件
            byte[]data1=DES_Use.getByte(name);
            BigInteger bigInteger=DES_Use.produceDEAKey();//生成密钥，返回密钥大整数。
            byte []data2=DES_Use.EncryptionDES(data1);//使用DES密钥加密，返回密文A。
            DES_Use.DecryptionDES(data2);
            RSA_Use.produceRSAKey();
            BigInteger c=RSA_Use.EncryptionRSA(bigInteger);//使用RSA公钥对DES的密钥进行加密,密文B、公钥可公开

            //使用混合密码体制DES-RSA解密文件
            Key key=RSA_Use.DecryptionRSA(c);//使用RSA的私钥解密。得到DES的密钥
            DES_Use.DecryptionRSA_DES(data2,key);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
