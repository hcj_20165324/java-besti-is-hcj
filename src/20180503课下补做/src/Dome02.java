import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import java.util.TreeSet;

/*
1.定义链表保存学生英语成绩
2.遍历链表
3.英语成绩保存至树中
 */
public class Dome02 {
    public static void main(String[] args) {
        LinkedList<Munber> mylist=new LinkedList<Munber>();
        TreeSet<Integer> mytree=new TreeSet<Integer>();
        String name[]={"hcj","aaa","bbb","ccc"};
        int  score[]={45,99,78,98};
        Munber munber[]=new Munber[4];
        for(int i=0;i<4;i++) {
            munber[i]=new Munber(name[i],score[i]);
            mylist.add(munber[i]);
        }
        Iterator<Munber> iter=mylist.iterator();
        while (iter.hasNext()) {
            Munber m=iter.next();
            mytree.add((int) m.getEnglishScore());
        }
        Iterator<Integer> te=mytree.iterator();
        while (te.hasNext()) {
            int temp=te.next();
            System.out.println(temp);
        }
    }
}
class Munber implements Comparable {
    private String name;
    private int EnglishScore;
    Munber(String name,int EnglishScore) {
        this.name=name;
        this.EnglishScore=EnglishScore;
    }
    public String getName() {
        return name;
    }
    public int  getEnglishScore() {
        return  EnglishScore;
    }
    public int compareTo(Object b) {
        Munber st=(Munber) b;
        return (this.getEnglishScore()-st.getEnglishScore());
    }
}