import java.util.*;
public class Dome01 {
    public static void main(String[] args) {
        Stack<Integer> stack=new Stack<Integer>();
        int temp,m,n;
        stack.push(3);
        stack.push(8);
        System.out.println("输入需要打印的前n项的值(n至少为3)：");
        Scanner sc=new Scanner(System.in);
        int number=sc.nextInt();
        if(number>2){
            System.out.println(3);
            System.out.println(8);
        }
        for (int i = 0; i < number-2; i++) {
            m=stack.pop();//出栈8
            n=stack.pop();//出栈3
            temp = 2*(m+n);
            stack.push(n);//入栈3
            stack.push(m);//入栈8
            stack.push(temp);//入栈22
            System.out.println(temp);
        }
    }
}