package yan;
import java.util.*;
class StudentKey implements Comparable {
    double d=0;
    StudentKey (double d) {
        this.d=d;
    }
    public int compareTo(Object b) {
        StudentKey st=(StudentKey)b;
        if((this.d-st.d)==0)
            return -1;
        else
            return (int)((this.d-st.d)*1000);
    }
}
class Student  {
    private String name;
    private int ID;
    private double mathScore;
    private double englishScore;
    private double javaScore;
    private double sum = 0;
    Student(String name, int ID,double mathScore,double englishScore,double javaScore) {
        this.name = name;
        this.ID = ID;
        this.mathScore=mathScore;
        this.englishScore=englishScore;
        this.javaScore=javaScore;
    }
    public double getMathScore() {
        return mathScore;
    }
    public double getEnglishScore() {
        return englishScore;
    }
    public double getJavaScore(){
        return javaScore;
    }
    public double getSum() {
        return sum = mathScore + englishScore+javaScore;
    }
    public String getName() {
        return name;
    }
    public int getID() {
        return ID;
    }
}
public class Test1 {
    public static void main(String args[ ]) {
        TreeMap<StudentKey,Student>  treemap= new TreeMap<StudentKey,Student>();
        String str[]={"王瑶佳","杨金川","何春江","李东骏","陈卓"};
        int ID[]={22,23,24,25,26};
        double m[]={78,84,85,98,87};
        double p[]={79,77,87,89,91};
        double e[]={99,78,88,74,68};
        Student student[]=new Student[5];
        for(int k=0;k<student.length;k++) {
            student[k]=new Student(str[k],ID[k],m[k],p[k],e[k]);
        }
        StudentKey key[]=new StudentKey[5] ;
        for(int k=0;k<key.length;k++) {
            key[k]=new StudentKey(student[k].getID());
        }
        for(int k=0;k<student.length;k++) {
            treemap.put(key[k],student[k]);  //put方法添加结点
        }
        int number=treemap.size();
        System.out.println("按学号排序：:");
        Collection<Student> collection=treemap.values();//遍历
        Iterator<Student> iter=collection.iterator();//迭代
        while(iter.hasNext()) {
            Student stu=iter.next();
            System.out.println(stu.getName()+" ID："+stu.getID()+" 英语成绩："+stu.getEnglishScore()+" Java成绩:"+stu.getJavaScore()+" 数学成绩:"+stu.getMathScore());
        }
        treemap.clear();
        for(int k=0;k<key.length;k++) {
            key[k]=new StudentKey(student[k].getSum());
        }
        for(int k=0;k<student.length;k++) {
            treemap.put(key[k],student[k]);
        }
        number=treemap.size();
        System.out.println("按总成绩排序：:");
        collection=treemap.values();
        iter=collection.iterator();
        while(iter.hasNext()) {
            Student stu=(Student)iter.next();
            System.out.println(stu.getName()+" ID："+stu.getID()+" 总成绩："+stu.getSum());
        }
    }
}
