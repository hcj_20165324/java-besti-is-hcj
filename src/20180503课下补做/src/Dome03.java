/*
使用树
 */
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;
public class Dome03 {
    public static void main(String[] args) {
        TreeMap<Key,Yan> treeMap=new TreeMap<Key,Yan>();
        double  price[]={100,101,102,99,98,97,103,104,96,95};
        double capacity[]={45,65,25,78,58,47,88,13,24,77};

        Yan Upan[]=new Yan[10];
        Key key[]=new Key[10];
        for(int i=0;i<10;i++) {
            Upan[i]=new Yan(price[i],capacity[i]);
            key[i]=new Key(Upan[i].getCapacity());
            treeMap.put(key[i],Upan[i]);
        }
        int number=treeMap.size();
        System.out.println("按价格排序");
        Collection<Yan> collection=treeMap.values();
        Iterator<Yan> iter=collection.iterator();
        while(iter.hasNext()) {
            Yan yan=iter.next();
            System.out.println(yan.getPrice()+" "+yan.getCapacity());
        }
        treeMap.clear();
        for(int i=0;i<10;i++) {
            key[i]=new Key(Upan[i].getPrice());
            treeMap.put(key[i],Upan[i]);
        }
        System.out.println("按容量排序");
        collection=treeMap.values();
        iter=collection.iterator();
        while (iter.hasNext()) {
            Yan yan=iter.next();
            System.out.println(yan.getPrice()+" "+yan.getCapacity());
        }
    }
}
class Yan {
    private double capacity;
    private double price;

    Yan(double capacity, double price) {
        this.price = price;
        this.capacity = capacity;

    }

    public double getPrice() {
        return price;
    }

    public double getCapacity() {
        return capacity;
    }
}
class Key implements Comparable{
    double score=0;
    Key(double score) {
        this.score=score;
    }
    @Override
    public int compareTo(Object o) {
        Key key=(Key)o;
        if(this.score-((Key) o).score==0) {
            return -1;
        }
        else {
            return (int)(this.score-((Key) o).score)*1000;
        }
    }
}