/**
 * Created by 81052 on 2018/4/7.
 */
public class Dome04 {
    public static void main(String[] args) {
        double sum=0,item=0;
        boolean computable=true;
        for (String s:args) {
            try {
                item=Double.parseDouble(s);
                sum+=item;
            }
            catch(NumberFormatException e) {
                System.out.println("你输入了非数字的字符:"+e);
                computable=false;
            }
        }
        if (computable) {
            System.out.println("sum="+sum);
        }
    }
}
