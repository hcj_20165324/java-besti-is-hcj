/**
 * Created by 81052 on 2018/4/7.
 */
public class Dome02 {
    public static void main(String[] args) {
        String s1,s2;
        s1=new String("天天");
        s2=new String("天天");
        System.out.println(s1.equals(s2));
        System.out.println(s1==s2);
        String s3,s4;
        s3="we are students";
        s4=new String ("we are students");
        System.out.println(s3.equals(s4));
        System.out.println(s3==s4);
        String s5,s6;
        s5="引用";
        s6="引用";
        System.out.println(s5.equals(s6));
        System.out.println(s5==s6);
    }
}
