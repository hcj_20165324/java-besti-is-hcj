import java.util.Scanner;

/**
 * Created by 81052 on 2018/4/7.
 */
public class Dome08 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入需要加密的明文：");
        String sourceString=sc.nextLine();
        EncryptAndDecrypt person=new EncryptAndDecrypt();
        System.out.println("输入密码加密:");
        String password=sc.nextLine();
        String secret=person.encrypt(sourceString,password);
        System.out.println("密文:"+secret);
        System.out.println("输入解密密码:");
        password=sc.nextLine();
        String source=person.decrypt(secret,password);
        System.out.println("明文:"+source);
    }
}
