import java.util.StringTokenizer;

/**
 * Created by 81052 on 2018/4/9.
 */
public class Test1 {
    public static void main(String[] args) {
        String s="价格表：你5.2元，我13.14元，他2.33元";
        PriceToken lookPriceMess=new PriceToken();
        System.out.println(s);
        double sum=lookPriceMess.getPriceSum(s);
        System.out.printf("购物总价格为:%.2f",sum);
        int amount=lookPriceMess.getGoodsAmount(s);
        double aver=lookPriceMess.getAverPrice(s);
        System.out.printf("\n商品数量：%d,平均价格:%-7.2f\n",amount,aver);
        Price price=new Price();
        price.getNumber(s);
    }
}
class Price {
    public void getNumber(String s) {
        String regex="[^0123456789.]+";
        s=s.replaceAll(regex,"*");
        System.out.println(s);
        StringTokenizer fenxi=new StringTokenizer(s,"*");
        int n=fenxi.countTokens();
        double a[]=new double [n];
        String m;
        for (int i=0;i<n;i++) {
            m=fenxi.nextToken();
            a[i]=Double.parseDouble(m);
        }
        for(double yy:a) {
            System.out.println(yy);
        }
    }
}
