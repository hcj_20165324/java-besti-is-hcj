import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by 81052 on 2018/4/8.
 */
public class GetPrice {
    public static double givePriceSum(String cost) {
        Scanner sc=new Scanner(cost);
        sc.useDelimiter("[^0123456789.]+");
        double sum=0;
        while(sc.hasNext()) {
            try {
                double price=sc.nextDouble();
                sum+=price;
            }
            catch(InputMismatchException exp) {
                String t=sc.next();
            }
        }
        return sum;
    }
}
