import java.util.Scanner;

/**
 * Created by 81052 on 2018/4/7.
 */
public class Dome09 {
    public static void main(String[] args) {
        String regex="[a-zA-Z|0-9|_]+";
        Scanner sc=new Scanner(System.in);
        String str=sc.nextLine();
        if (str.matches(regex)) {
            System.out.println(str+"是由英文字母、数字或下划线构成");
        }
        else {
            System.out.println(str+"中有非法字符");
        }
    }
}
