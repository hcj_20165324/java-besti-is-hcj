import java.util.Calendar;
import java.util.Date;

/**
 * Created by 81052 on 2018/4/10.
 */
public class Dome15 {
    public static void main(String[] args) {
        Calendar calendar=Calendar.getInstance();//初始化一个日历对象
        calendar.setTime(new Date());
        int year=calendar.get(Calendar.YEAR),
                month=calendar.get(Calendar.MONTH)+1,
                day=calendar.get(Calendar.DAY_OF_MONTH),
                hour=calendar.get(Calendar.HOUR_OF_DAY),
                minute=calendar.get(Calendar.MINUTE),
                second=calendar.get(Calendar.SECOND);
        System.out.print("现在的时间为:");
        System.out.print(""+year+"年"+month+"月"+day+"日");
        System.out.println(" "+hour+"时"+minute+"分"+second+"秒");
        int y=2012,m=9,d=1;
        calendar.set(y,m-1,d);
        long time1=calendar.getTimeInMillis();
        y=2016;
        m=7;
        d=1;
        calendar.set(y,m-1,d);
        long time2=calendar.getTimeInMillis();
        long subDay=(time2-time1)/(1000*60*60*24);
        System.out.println(""+new Date(time2));
        System.out.println("与"+new Date(time1));
        System.out.println("相隔"+subDay+"天");
    }
}
