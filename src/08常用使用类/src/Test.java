public class Test {
    public static void main(String args[]) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        Oper oper=new Oper();
        try {
             int h=oper.C(a, b);
            System.out.println(h);
        }
        catch (YYException e) {
            System.out.println(e.warnMess());
        }

    }
}
class Oper{
    public static int C(int n,int m)throws YYException {
        if(n<=0||m<=0||m>n) {
            throw new YYException();
        }
        else if(n==m) {
            return 1;
        }
        else if(m==1) {
            return n;
        }
        else {
            return C(n-1,m-1)+C(n-1,m);
        }
    }
}
class YYException extends Exception {
    String massage="不符合该运算";
    public String warnMess() {
        return massage;
    }
}