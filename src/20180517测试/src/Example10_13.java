import java.io.*;
public class Example10_13 {
    public static void main(String args[]) {
        TV changhong = new TV();
        changhong.setName("长虹电视");
        changhong.setPrice(5678);
        File file=new File("television.txt");
        try{
            FileOutputStream fileOut=new FileOutputStream(file);
            ObjectOutputStream objectOut=new ObjectOutputStream(fileOut);
            objectOut.writeObject(changhong);
            objectOut.close();
            FileInputStream fileIn=new FileInputStream(file);
            ObjectInputStream objectIn=new ObjectInputStream(fileIn);
            TV haier=(TV)objectIn.readObject();
            objectIn.close();
            haier.setName("海尔电视");
            haier.setPrice(5324);
            System.out.println("changhong名字:"+changhong.getName());
            System.out.println("changhong的价格:"+changhong.getPrice());
            System.out.println("haier名字:"+haier.getName());
            System.out.println("haier的价格:"+haier.getPrice());
        }
        catch(ClassNotFoundException event) {
            System.out.println("不能读出对象");
        }
        catch(IOException event) {
            System.out.println(event);
        }
    }
}
