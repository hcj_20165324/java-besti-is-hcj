import java.io.*;

public class GenNumber {
    public static void main(String[] args) {
        int [] a =GetRandomNumber.getRandomNumber(5324,324);
        File file=new File("20165324.txt");
        try {
            FileOutputStream out=new FileOutputStream(file);
            DataOutputStream outData=new DataOutputStream(out);
            for(int i=0;i<324;i++) {
                outData.writeInt(a[i]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}