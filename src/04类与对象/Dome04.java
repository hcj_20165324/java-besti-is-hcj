/*在主类的main方法中使用Computer类来创建对象，
该对象可以调用add(int x,int y)方法计算两个整数的和*/
class Computer{
	int add(int x,int y){
		return x+y;
	}
}
public class Dome04 {
	public static void main(String[] args) {
		Computer com=new Computer();
		int m=100;
		int n=200;
		int result=com.add(m,n);//对象调用类中的方法格式为：对象.方法;
		System.out.println(result);
		result=com.add(120+m,n*10+8);
		System.out.println(result);
	}
}
