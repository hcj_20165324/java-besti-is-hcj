//定义一个类InputScore：录入分数。调用DelScore
import java.util.Scanner;
public class InputScore { 
	DelScore del;
	InputScore(DelScore del) {
		this.del=del;
	}
	public void inputScore() {
	System.out.println("请输入评委数");
	Scanner sc=new Scanner(System.in);
	int count=sc.nextInt();
	System.out.println("请输入各个评委的分数");
	double []a=new double[count];
	for (int i=0;i<count;i++){
		a[i]=sc.nextDouble();
	}
	del.doDelete(a);
	}
}
