#include<stdio.h>
#define N 40
typedef struct student{
          int ID;
          char name[10];
          char gender[3];
          int mathScore;
          int phyScore;
}Student;
void Input(Student stu[],int n);
void Sort(Student stu[],int n);
void Output(Student stu[],int n);
int main()
{
          int m,n;
          scanf("%d %d\n",&m,&n);
          Student stu1[N];
          Student stu2[N];
          Input(stu1,m);
          Input(stu2,n);
          Sort(stu1,m);
          Sort(stu2,n);
          Output(stu1,m);
          Output(stu2,n);
          return 0;
}
void Input(Student stu[],int n){
          int i;
          for(i=0;i<n;i++) {
                    scanf("%d%s%s%d%d",&stu[i].ID,stu[i].name,stu[i].gender,&stu[i].mathScore,&stu[i].phyScore);
          }
}
void Sort(Student stu[],int n){
          int i,j;
          for(i=0;i<n-1;i++) {
                    for(j=i+1;j<n;j++) {
                              if(stu[i].ID>stu[j].ID) {
                                        Student s=stu[i];
                                        stu[i]=stu[j];
                                        stu[j]=s;
                              }
                    }
          }
}
void Output(Student stu[],int n){
          int i;
          for(i=0;i<n;i++) {
                    printf("%d %s %s %d %d ",stu[i].ID,stu[i].name,stu[i].gender,stu[i].mathScore,stu[i].phyScore);
          }
          printf("\n");
}

