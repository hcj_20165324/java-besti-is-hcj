import java.util.Scanner;
public class Test3 {
	public static void main(String args[]) {
		System.out.println("输入两个班的人数:");
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int m=sc.nextInt();
		Student []stu1=new Student[n];//创建对象数组
		Student []stu2=new Student[m];//创建对象数组
		Fen(stu1,n);
		Fen(stu2,m);
		System.out.println("现在输入一班学生信息：");
		Input(stu1,n);
		System.out.println("现在输入二班学生信息：");
		Input(stu2,m);
		paixu(stu1,n);
		paixu(stu2,m);
		System.out.println("一班信息为：");
		Output(stu1,n);
		System.out.println("二班信息为：");
		Output(stu2,m);
	}
	static void Fen(Student stu[],int n) {//分配空间
		for (int i=0;i<n;i++) {
			stu[i]=new Student();
		}
	}
		
	static void Input(Student stu[],int n) {//输入
		Scanner sc=new Scanner(System.in);
		for (int i=0;i<n;i++) {
			System.out.println("请按以下格式输入学生信息：姓名 学号 性别 数学成绩 物理成绩");
			stu[i].name=sc.next();
			stu[i].number=sc.nextInt();
			stu[i].gender=sc.next();
			stu[i].mathScore=sc.nextInt();
			stu[i].phyScore=sc.nextInt();
		}
	}
	static void paixu(Student stu[],int n) {	
		for (int i=0;i<n-1;i++) {
			for (int j=i+1;j<n;j++) {
				if (stu[i].number>stu[j].number) {
					Student s=stu[i];					
					stu[i]=stu[j];
					stu[j]=s;
				}
			}
		}
	}
	static void Output(Student stu[],int n) {
		for (int i=0;i<n;i++) {
			System.out.println("姓名："+stu[i].name+"，学号："+stu[i].number+"，性别："+stu[i].gender+"，数学成绩："+stu[i].mathScore+"，物理成绩："+stu[i].phyScore);
		}
	}
}
class Student {
	String name;
	int number;
	String gender;
	int mathScore;
	int phyScore;
}
