class A {
	Object get() {
		return null;
	}
}
class B extends A {
	Integer get() {
		return new Integer (100);
	}
}
public class Dome06 {
	public static void main(String args[]) {
		B b=new B();
		Integer t=b.get();
		System.out.println(t.intValue());
	}
}