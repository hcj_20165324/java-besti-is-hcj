interface ShowMessage {
	void 显示商标(String s);
}
class TV implements ShowMessage {
	public void 显示商标(String s) {
		System.out.println(s);
	}
}
class PC implements ShowMessage {
	public void 显示商标(String s) {
		System.out.println(s);
	}
}
public class Dome02 {
	public static void main(String args[]) {
		ShowMessage sm;
		sm=new TV();
		sm.显示商标("长城");
		sm=new PC();
		sm.显示商标("联想");
	}
}