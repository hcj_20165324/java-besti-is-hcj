abstract class MotorVehicles { //抽象类，关键字为abstract,封装了行为标准
	abstract void brake();
	/*抽象类只能定义常量、抽象方法。并且abstract方法必须为实例方法,
	  实例方法是指方法类型前面不加关键字static修饰的方法*/
}
interface MoneyFare { //接口MoneyFare
	void charge();    //接口中的方法，在类中必须重写
}
interface ControlTemperature { //接口ControlTemperature
	void controlAirTemperature();//接口中的方法，在类中必须重写
}
class Bus extends MotorVehicles implements MoneyFare { //子类Bus，实现接口MoneyFare
	void brake() { //抽象类方法的引用
		System.out.println("公共汽车技术");
	}
	public void charge() { //重写接口的方法，并且访问权限必须为public
		System.out.println("公共汽车：一元/张，不计公里数");
	}
}
class Taxi extends MotorVehicles implements MoneyFare,ControlTemperature {//子类Taxi,实现接口MoneyFare,ControlTemperature
	void brake() { //抽象类方法的引用
		System.out.println("出租车使用技术");
	}
	public void charge() { //重写接口的方法，并且访问权限必须为public
		System.out.println("出租车：2元/公里，起步价3公里");
	}
	public void controlAirTemperature() { //重写接口的方法，并且访问权限必须为public
		System.out.println("出租车安装了空调");
	}
}
class Cinema implements MoneyFare,ControlTemperature { //类Cinema，实现接口MoneyFare,ControlTemperature
	public void charge() { //重写接口的方法，并且访问权限必须为public
		System.out.println("电影院：门票，10元/张");
	}
	public void controlAirTemperature() { //重写接口的方法，并且访问权限必须为public
		System.out.println("电影院安装了空调");
	}
}
public class Dome03 {
	public static void main(String args[]) {
		Bus bus101=new Bus();//创建对象：对象的声明、分配变量。
		Taxi buleTaxi=new Taxi();//创建对象：对象的声明、分配变量。
		Cinema redStarCinema=new Cinema();//创建对象：对象的声明、分配变量。

		MoneyFare fare;//接口回调，声明一个变量，此时fare为一个空接口
		ControlTemperature temperature;//接口回调，声明一个变量，此时temperature为一个空接口

		fare=bus101;//将上述bus101对象的引用赋值给fare接口变量
		bus101.brake();//类所声明的方法的调用
		fare.charge();//fare接口可以调用类实现的接口方法，即charge方法

		fare=buleTaxi;//将上述buleTaxi对象的引用赋值给fare接口变量
		temperature=buleTaxi;//将上述buleTAxi对象的引用赋值给temperature接口变量
		buleTaxi.brake();//类所声明的方法的调用
		fare.charge();//fare接口可以调用类实现的接口方法，即charge方法
		temperature.controlAirTemperature();//temperature接口可以调用类实现的接口方法，即controlAirTemperature方法

		fare=redStarCinema;//将上述redStarCinema对象的引用赋值给fare接口变量
		temperature=redStarCinema;//将上述redStarCinema对象的引用赋值给temperature接口变量
		fare.charge();//fare接口可以调用类实现的接口方法，即charge方法
		temperature.controlAirTemperature();//temperature接口可以调用类实现的接口方法，即controlAirTemperature方法
	}
}