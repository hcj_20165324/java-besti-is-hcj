interface SpeakHello {
	void speakHello();
}
class Chinese implements SpeakHello {
	public void speakHello() {
		System.out.println("你好");
	}
}
class English implements SpeakHello {
	public void speakHello() {
		System.out.println("Hello");
	}
}
class KindHello {
	public void lookHello(SpeakHello hello) {// 接口类型参数
		hello.speakHello();//接口回调
	}
}
public class Dome05 {
	public static void main(String args[]) {
		KindHello kindHello=new KindHello();
		kindHello.lookHello(new Chinese());
		kindHello.lookHello(new English());
	}
}