public class Example12_2 { 
   public static void main(String args[]) { 
       Thread speakElephant;            //用Thread声明线程
       Thread speakCar;                 //用Thread声明线程
       ElephantTarget elephant;         //elephant是目标文件
       CarTarget car;                   //Car是目标文件
       elephant = new ElephantTarget();//创建目标文件
       car = new CarTarget();           //创建目标文件
       speakElephant = new Thread(elephant) ;   //创建线程，其目标对象是elephant
       speakCar = new Thread(car);              //创建线程，其目标对象是car
       speakElephant.start();                    //启动线程
       speakCar.start();                        //启动线程
       for(int i=1;i<=15;i++) {
          System.out.print("主人"+i+"  ");
       }  
   }
}
