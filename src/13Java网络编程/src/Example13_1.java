import java.net.*;
import java.io.*;
import java.util.*;
public class Example13_1 {
   public static void main(String args[]) {
       Scanner scanner;
       URL url;//创建URL对象
       Thread readURL;//负责读取资源的线程，因为URL资源的读取可能会引起阻塞，需要在一个线程中读取。
       Look look = new Look();//线程的目标对象
       System.out.println("输入URL资源，例如:https://home.cnblogs.com/u/yh666/");
       scanner = new Scanner(System.in);
       String source = scanner.nextLine();
       try {  url = new URL(source);
              look.setURL(url);
              readURL = new Thread(look);
              readURL.start();
       }
       catch(Exception exp){
          System.out.println(exp);
       } 
   }
}

