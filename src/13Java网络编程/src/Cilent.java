import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Cilent {
    public static void main(String[] args) {
        String [] mess= {"2010世界杯在哪举行？","巴西进世界杯了吗？","中国进入世界杯了吗？"};
        Socket mysocket;//客服端程序使用Socket类建立负责连接到服务器的套接字对象
        DataInputStream in=null;
        DataOutputStream out=null;
        try {
            mysocket=new Socket("172.30.2.248",5315);//参数host为服务器的IP地址，port是一个端口号
            in=new DataInputStream(mysocket.getInputStream());//对象调用getInputStream()，将客户端获得的输入流指定为服务器端的输出流
            out=new DataOutputStream(mysocket.getOutputStream());//对象调用getOutputStream()，将客户端获得的输出流指定为服务器端的输入流
            for(int i=0;i<mess.length;i++) {
                out.writeUTF(mess[i]);
                String s=in.readUTF();
                System.out.println("客户收到了服务器的回答："+s);
                Thread.sleep(500);
            }
        }
        catch(Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}
