import java.net.InetAddress;
import java.net.UnknownHostException;

public class Example13_2 {
    public static void main(String[] args) {
        try {
            //获取域名为www.sina.com.cn的主机域名及IP地址
            InetAddress address_1=InetAddress.getByName("www.sina.com.cn");
            System.out.println(address_1.toString());
            //获取IP为202.108.22.5 的主机域名及IP地址
            InetAddress address_2=InetAddress.getByName("202.108.22.5");
            System.out.println(address_2.toString());
            //获取本地机器的域名和IP地址
            InetAddress address_3=InetAddress.getLocalHost();
            System.out.println(address_3.toString());

        }catch (UnknownHostException e) {
            System.out.println("无法找到");
        }
    }
}
