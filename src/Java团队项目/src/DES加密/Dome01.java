package DES加密;/*
文件转换为byte[]数组，并使用DES進行加密
密钥生成器DES
 */
import javax.crypto.Cipher;
import java.io.*;
import java.security.Key;
import java.util.Scanner;

public class Dome01 {
    public static void main(String[] args) {
        try {
            System.out.println("输入需要加密的文件名：");
            Scanner sc=new Scanner(System.in);
            String fileName=sc.nextLine();
            FileInputStream fis = new FileInputStream(fileName);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[99999999];
            int len = 0;
            // 将内容读到buffer中，读到末尾为-1
            while ((len = fis.read(buffer)) != -1) {
                // 本例子将每次读到字节数组(buffer变量)内容写到内存缓冲区中，起到保存每次内容的作用
                outStream.write(buffer, 0, len);
            }
            byte[] data = outStream.toByteArray(); // 取内存中保存的数据
            fis.close();
            String result = new String(data, "UTF-8");

            FileInputStream f=new FileInputStream("key1.dat");
            ObjectInputStream b=new ObjectInputStream(f);
            Key k=(Key)b.readObject( );//从文件中获取密钥
            Cipher cp=Cipher.getInstance("DESede");//创建密码器
            cp.init(Cipher.ENCRYPT_MODE, k);//初始化密码器，第一个参数指定密码器准备进行加密还是解密，第二个参数则传入加密或解密所使用的密钥。
            byte ctext[]=cp.doFinal(data);//执行Cipher对象的doFinal()方法，该方法的参数中传入待加密的明文，从而按照前面设置的算法与模式对明文加密。
            System.out.println("密文为：");
            for(int i=0;i<ctext.length;i++){
                System.out.print(ctext[i] +",");
            }
            FileOutputStream f2=new FileOutputStream("SEnc.dat");//将密文保存于SEnc.dat中。
            f2.write(ctext);//处理加密结果
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}

