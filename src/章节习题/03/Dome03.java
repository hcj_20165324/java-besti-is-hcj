//分别用do-while和for循环计算1+1/2！+1/3！+1/4！+...前二十项
public class Dome03 {
	public static void main(String[] args) {
		double sum=0;
		double i=1;
		double j=1;
		do{
		    j=j*(1/i);
			i++;
			sum=sum+j;
		}
		while (i<=20);
		System.out.println(sum);
	}
}
class dome03{
	public static void main(String args[]){
		double sum=0;
		double j=1;
		for(double i=1;i<=20;i++){
			j=j*(1/i);
			sum=sum+j;
		}
		System.out.println(sum);
	}
}
