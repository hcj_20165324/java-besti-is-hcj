abstract class Animal {
	abstract void cry();
	abstract String getAnimalName();
}
class Cat extends Animal {
	public void cry() {
		System.out.println("��������");
	}
	public String getAnimalName() {
		return "è";
	}
}
class Dog extends Animal {
	public void cry() {
		System.out.println("��������");
	}
	public String getAnimalName() {
		return "��";
	}
}
class Simulator {
	public void playSound(Animal animal) {
		System.out.println("���������Ϊ"+animal.getAnimalName());
		animal.cry();
	}
}
public class Application {
	public static void main(String args[]) {
		Simulator simulator=new Simulator();
		simulator.playSound(new Dog());
		simulator.playSound(new Cat());
	}
}