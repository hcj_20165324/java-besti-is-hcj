//用户在键盘依次输入若干数字，回车确定，最后输入一个非数字的字符串结束过程，计算和及平均值。
import java.util.Scanner;
public class Dome01 {
	public static void main(String[] args) {
		System.out.println("依次输入若干数字，回车确定，最后输入一个非数字的字符串结束过程");
		Scanner sc=new Scanner(System.in);
		double sum=0;
		double aver=0;
		int count=0;
		while (sc.hasNextDouble()){
			double dd=sc.nextDouble();
			assert (dd<100&&dd>0):"非法的成绩数据";
			sum=sum+dd;
			count++;
		}
		aver=sum/count;
		System.out.println("sum="+sum);
		System.out.println("aver="+aver);
	}
}
