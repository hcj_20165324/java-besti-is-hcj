interface Animal {
	void cry();
	String getAnimalName();
}
class Dog implements Animal {
	public void cry() {
		System.out.println("��������");
	}
	public String getAnimalName() {
		return "��";
	}
}
class Cat implements Animal {
	public void cry() {
		System.out.println("��������");
	}
	public String getAnimalName() {
		return "è";
	}
}
class Simulator {
	public void playSound(Animal animal) {
		System.out.println("���������Ϊ"+animal.getAnimalName());
		animal.cry();
	}
}
public class Application {
	public static void main(String args[]) {
		Simulator simulator=new Simulator();
		simulator.playSound(new Dog());
		simulator.playSound(new Cat());
	}
}