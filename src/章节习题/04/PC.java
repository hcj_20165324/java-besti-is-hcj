/*
//CPU类
public class CPU{
	int speed;
	void setSpeed(int a) {
		speed=a;
	}
	int getSpeed() {
		return speed;
	}
}
*/
//PC类，在PC上读取CPU速度，读取硬板的容量
public class PC{
	CPU cpu;
    HardDisk HD;
	void setCPU(CPU c) {
		cpu=c;
	}
	void setHardDisk(HardDisk h) {
		HD=h;
	}
	void show() {
		System.out.println(cpu.getSpeed());
		System.out.println(HD.getAmount());
	}
}