/*
完成教材110页编程题，额外要求
1. 除了Test类，PC，CPU，HardDisk要实现多个构造方法，比如PC类要有PC(), PC(CPU cpu), PC(HardDisk HD), PC(CPU cpu, HardDisk HD)
2. 除了Test类，PC，CPU，HardDisk要覆盖toString(), equals()方法
3. Test类中要测试到所有类的toString(), equals()方法
4. 提交Test类运行的截图
5. 把代码推送到码云
*/
//主类Test
public class Test {
	public static void main(String[] args) {
		CPU cpu=new CPU();
		cpu.setSpeed(2200);
		HardDisk disk=new HardDisk();
		disk.setAmount(200);
		PC pc=new PC();
		pc.cpu=cpu;
		pc.HD=disk;
		pc.show();
	}
}
