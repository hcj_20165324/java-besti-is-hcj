import java.security.*;
import java.util.Scanner;

public class DigestPass{
    public static void main(String args[ ]) throws Exception{
        //String x=args[0];使用命令行输入
        Scanner sc=new Scanner(System.in);
        String x=sc.nextLine();
        MessageDigest m=MessageDigest.getInstance("MD5");//生成MessageDigest对象,使用静态方法
        m.update(x.getBytes("UTF8"));//x为需要计算的字符串，update传入的参数是字节类型或字节类型数组，对于字符串，需要先使用getBytes( )方法生成字符串数组。
        byte s[ ]=m.digest( );//计算的结果通过字节类型的数组返回。
        String result="";
        for (int i=0; i<s.length; i++){
            //处理计算结果
            result+=Integer.toHexString((0x000000ff & s[i]) |
                    0xffffff00).substring(6);
        }
        System.out.println(result);
    }
}