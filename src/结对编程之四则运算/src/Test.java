import java.io.*;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;
public class Test {
    public static void main(String[] args) {
        int op1, op2, result = 0;
        String token;
        System.out.println("请输入需要测试题的数目：");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int count = 0;//计数
        Stack<Integer> stack = new Stack<Integer>();
        File file = new File("四则运算.txt");
        try {
            Reader in = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(in);
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                count++;
                if (count==number) {
                    return;
                }
                StringTokenizer fenxi = new StringTokenizer(str);
                while (fenxi.hasMoreTokens()) {
                    token = fenxi.nextToken();
                    if (isOperator(token)) {
                        op1 = stack.pop().intValue();
                        op2 = stack.pop().intValue();
                        result = evalSingleOp(token.charAt(0), op1, op2);
                        stack.push(new Integer(result));
                    } else {
                        stack.push(new Integer(Integer.parseInt(token)));
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
    private static int evalSingleOp(char operation, int op1, int op2) {
        int result = 0;
        switch (operation)
        {
            case '+':
                result = op1 + op2;
                break;
            case '-':
                result = op1 - op2;
                break;
            case '*':
                result = op1 * op2;
                break;
            case '/':
                result = op1 / op2;
        }
        return result;
    }
    private static boolean isOperator(String token) {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/") );
    }
}



