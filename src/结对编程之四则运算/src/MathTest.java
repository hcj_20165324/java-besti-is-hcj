import java.util.Random;
import java.util.Scanner;

public class MathTest {

    public static void main(String[] args) {

        double right = 0;
        double wrong = 0;
        for (int i = 0; i < 10; i++) {
            System.out.println("第"+(i+1)+"道题,请在下方输入答案：");
            //生成两个随机数
            Random r1 = new Random();
            int x = r1.nextInt(10)+1;
            Random r2 = new Random();
            int y = r2.nextInt(10)+1;
            //生成随机运算符    + - * /
            Random r3 = new Random();
            int z = r3.nextInt(4);
            char[] chs = {'+','-','*','/'};
            String Operator = String.valueOf(chs[z]);
            //生成题目
            if(Operator.equals("+")){
                System.out.println(x+"+"+y+"=");
                boolean b = add(x,y);
                if(b == true){
                    right++;System.out.println("right! ");
                }else{
                    wrong++;System.out.println("no ");
                }
                System.out.println("right answer: "+(x+y));

            }else if(Operator.equals("-")){
                System.out.println(x+"-"+y+"=");
                boolean b =minus(x,y);
                if(b == true){
                    right++;System.out.println("right! ");
                }else{
                    wrong++;System.out.println("no ");
                }
                System.out.println("right answer: "+(x-y));

            }else if(Operator.equals("*")){
                System.out.println(x+"×"+y+"=");
                boolean b =times(x,y);
                if(b == true){
                    right++;System.out.println("right! ");
                }else{
                    wrong++;System.out.println("no ");
                }
                System.out.println("right answer: "+(x*y));
            }else{
                System.out.println(x+"÷"+y+"=");
                boolean b =divide(x,y);
                if(b == true){
                    right++;System.out.println("right! ");
                }else{
                    wrong++;System.out.println("no ");
                }
                System.out.println("right answer: "+((float)x /(float) y));
            }
            System.out.println("-------------------------------");
        }
        System.out.println("做对了"+right+"道题.");
        System.out.println("做错了"+wrong+"道题.");
        if(wrong > 0){
            System.out.println("正确率为"+(right/(wrong+right))*100+"%");
        }else{
            System.out.println("没法除了");
        }
    }

    private static boolean add(int x,int y) {
        Scanner sc = new Scanner(System.in);
        int num1 = sc.nextInt();
        int result = x + y;
        if(num1 == result){
            return true;
        }else{
            return false;
        }

    }
    private static boolean minus(int x,int y) {
        Scanner sc = new Scanner(System.in);
        int num1 = sc.nextInt();
        int result = x - y;
        if(num1 == result){
            return true;
        }else{
            return false;
        }

    }
    private static boolean times (int x,int y) {
        Scanner sc = new Scanner(System.in);
        int num1 = sc.nextInt();
        int result = x * y;
        if(num1 == result){
            return true;
        }else{
            return false;
        }

    }
    private static boolean divide(int x,int y) {
        Scanner sc = new Scanner(System.in);
        float num1 = sc.nextFloat();
        float result =(float)x /(float) y;
        if(num1 == result){
            return true;
        }else{
            return false;
        }

    }

}
