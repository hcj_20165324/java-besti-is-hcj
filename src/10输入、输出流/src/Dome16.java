import java.io.File;
import java.util.Scanner;

/**
 * Created by 81052 on 2018/4/12.
 */
public class Dome16 {
    public static void main(String[] args) {
        File file=new File("student.txt");
        Scanner sc=null;
        int count=0;
        double sum=0;
        try{
            double score=0;
            sc=new Scanner(file);
            sc.useDelimiter("[^0123456789.]+");
            while (sc.hasNextDouble()) {
                score=sc.nextDouble();
                count++;
                sum+=score;
                System.out.println(score);
            }
            double aver=sum/count;
            System.out.println("平均成绩："+aver);
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
