import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by 81052 on 2018/4/11.
 */
public class Dome09 {
    public static void main(String[] args) {
        RandomAccessFile in=null;
        try{
            in=new RandomAccessFile("Dome09.java","rw");
            long length=in.length();
            long position=0;
            in.seek(position);
            while(position<length) {
                String str=in.readLine();
                byte b[]=str.getBytes("iso-8859-1");
                str=new String(b);
                position=in.getFilePointer();
                System.out.println(str);
            }
        }
        catch(IOException e){}
    }
}
