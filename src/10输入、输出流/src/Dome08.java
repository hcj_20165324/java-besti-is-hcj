import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by 81052 on 2018/4/11.
 */
public class Dome08 {
    public static void main(String[] args) {
        RandomAccessFile inAndOut=null;
        int date[] ={1,2,3,4,5,6,7,8,9,10};
        try {
            inAndOut =new RandomAccessFile("tom.dat","rw");
            for (int i=0;i<date.length;i++) {
                inAndOut.writeInt(date[i]);
            }
            for (long i=date.length-1;i>=0;i--) {
                inAndOut.seek(i*4);
                System.out.printf("\t%d",inAndOut.readInt());
            }
            inAndOut.close();
        }
        catch(IOException e) {}
    }
}
