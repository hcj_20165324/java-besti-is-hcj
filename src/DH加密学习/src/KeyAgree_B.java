import java.io.*;
import java.math.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;

public class KeyAgree_B {
    public static void main(String args[ ]) throws Exception{
        // 读取对方的DH公钥:Apub.dat
        FileInputStream f1=new FileInputStream(args[0]);
        ObjectInputStream b1=new ObjectInputStream(f1);
        PublicKey  pbk=(PublicKey)b1.readObject( );
        //读取自己的DH私钥:Bpri.dat
        FileInputStream f2=new FileInputStream(args[1]);
        ObjectInputStream b2=new ObjectInputStream(f2);
        PrivateKey  prk=(PrivateKey)b2.readObject( );
        // 执行密钥协定
        KeyAgreement ka=KeyAgreement.getInstance("DH");
        ka.init(prk);
        ka.doPhase(pbk,true);
        //生成共享信息
        byte[ ] sb=ka.generateSecret();
        for(int i=0;i<sb.length;i++){
            System.out.print(sb[i]+",");
        }
        //生成的密钥k为DESede类型的密钥
        SecretKeySpec k=new  SecretKeySpec(sb,"DESede");
        FileOutputStream  f=new FileOutputStream("B_Key_DESede_DH.dat");//指定产生密钥输出流文件
        ObjectOutputStream b=new  ObjectOutputStream(f);//将对象序列化，以流的方式进行处理
        b.writeObject(k);//通过以对象序列化方式将密钥保存在文件中
    }
}  