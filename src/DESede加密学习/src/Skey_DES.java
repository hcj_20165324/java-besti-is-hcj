/*
将密钥通过对象序列化方式保存在文件中，在文件中保存的是对象。
 */
import java.io.*;
        import javax.crypto.*;
public class Skey_DES{
    public static void main(String args[])
            throws Exception{
        KeyGenerator kg=KeyGenerator.getInstance("DESede");//1.获取密钥生成器
        kg.init(168);//初始化密钥生成器，DESede算法密钥长度为112,168位。DES算法密钥长度必须为56位
        SecretKey k=kg.generateKey( );//生成密钥
        FileOutputStream  f=new FileOutputStream("key1.dat");//指定产生密钥输出流文件
        ObjectOutputStream b=new  ObjectOutputStream(f);//将对象序列化，以流的方式进行处理
        b.writeObject(k);//通过以对象序列化方式将密钥保存在文件中
    }
}