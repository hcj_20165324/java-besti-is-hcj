/*
本实例的输入是面生成并以对象方式保存在文件key1.dat中的密钥，
以及需要加密的一段最简单的字符串"Hello World!"，
使用密钥对"Hello World!"进行加密，加密后的信息保存在文件中。
 */
import java.io.*;
import java.security.*;
import javax.crypto.*;
public class SEnc{
    public static void main(String args[]) throws Exception{
        String s="Hello World!";
        FileInputStream f=new FileInputStream("key1.dat");
        ObjectInputStream b=new ObjectInputStream(f);
        Key k=(Key)b.readObject( );//从文件中获取密钥
        Cipher cp=Cipher.getInstance("DESede");//创建密码器
        cp.init(Cipher.ENCRYPT_MODE, k);//初始化密码器，第一个参数指定密码器准备进行加密还是解密，第二个参数则传入加密或解密所使用的密钥。
        byte ptext[]=s.getBytes("UTF8");//Cipher对象所做的操作是针对字节数组，由此需要将加密的内容转换为字节数组
        System.out.println("初始，明文内容转换为字节数组");
        for(int i=0;i<ptext.length;i++){
            System.out.print(ptext[i]+",");
        }
        System.out.println(" ");
        System.out.println("加密，密文内容为字节数组");
        byte ctext[]=cp.doFinal(ptext);//执行Cipher对象的doFinal()方法，该方法的参数中传入待加密的明文，从而按照前面设置的算法与模式对明文加密。
        for(int i=0;i<ctext.length;i++){
            System.out.print(ctext[i] +",");
        }
        FileOutputStream f2=new FileOutputStream("SEnc.dat");
        f2.write(ctext);//处理加密结果
    }
}
/*
首先要从文件中获取已经生成的密钥，
然后考虑如何使用密钥进行加密。
这涉及到各种算法。Java中已经提供了常用的加密算法，
我们执行Java中Cipher类的各个方法就可以完成加密过程
 */

/*
第一行的内容没有加过密，任何人若得到第一行数据，
只要将其用二进制方式写入文本文件，用文本编辑器打开文件就可以看到对应的字符串“Hello World!”。
 */