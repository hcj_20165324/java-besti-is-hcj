import java.util.Scanner;
public class Test2 {
    public static void main(String[] args) {
        System.out.println("输入基数b:");
        Scanner sc=new Scanner(System.in);
        int b=sc.nextInt();
        for(int n=1;n<99999;n++) {
            Operate(b,n);
        }
    }
    private static void Operate(int b,int n ) {
        for(int i=1;i<n-1;i++) {
            b=b*b;
        }
        int r=b%n;
            while(r!=0){
                b=n;
                n=r;
                r=b%n;
            }
            if(r==1) {
            System.out.println("基数b为："+b+"，则最小的费马伪素数为:"+b);
        }
    }
}
/*
//**************************
//Fast Exponent Calculate
//**************************

#include <stdio.h>
#include <math.h>

int coprime(int x, int y){
//
    int r=x%y;
    while(r!=0){
        x=y;
        y=r;
        r=x%y;
    }
    if(y==1) return 1;
    return 0;
}

int fermat(int x, int y){
//
    int i, res=1;
    for(i=1; i<y; ++i){
        res*=x;
        res%=y;
    }
    if(res%y==1) return 0;
    return 1;
}

int main(){
//
    int b, n;
    scanf("%d", &n);
    for(b=2; b<n; ++b){
        if(coprime(b, n)){
            if(fermat(b, n)){
                printf("This is a composite number.\n");
                printf("%d, %d", b, n);
                return 0;
            }
        }
    }
    printf("This is a prime number.\n");
    return 0;
}

 */