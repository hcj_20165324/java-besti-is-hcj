/*
使用树
 */
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;
public class Dome02 {
    public static void main(String[] args) {
        TreeMap<Key,Yan> treeMap=new TreeMap<Key,Yan>();
        String name[]={"王瑶佳","杨金川","何春江","李东骏","陈卓"};
        int ID[]={22,23,24,25,26};
        double math[]={45,56,57,54,65};
        double english[]={78,74,86,98,78};
        Yan han[]=new Yan[5];
        Key key[]=new Key[5];
        for(int i=0;i<5;i++) {
            han[i]=new Yan(name[i],ID[i],math[i],english[i]);
            key[i]=new Key(han[i].getMathScore());//关键字按照数学成绩排序
            treeMap.put(key[i],han[i]);
        }
        int number=treeMap.size();
        System.out.println("按数学成绩排序");
        Collection<Yan> collection=treeMap.values();
        Iterator<Yan> iter=collection.iterator();
        while(iter.hasNext()) {
            Yan yan=iter.next();
            System.out.println(yan.getName()+" "+yan.getID()+" "+yan.getMathScore());
        }
        treeMap.clear();
        for(int i=0;i<5;i++) {
            key[i]=new Key(han[i].getSum());
            treeMap.put(key[i],han[i]);
        }
        System.out.println("按总成绩排序");
        collection=treeMap.values();
        iter=collection.iterator();
        while (iter.hasNext()) {
            Yan yan=iter.next();
            System.out.println(yan.getName()+" "+yan.getID()+" "+yan.getSum());
        }
    }
}
class Yan  {
    private String name;
    private int ID;
    private double mathScore;
    private double englishScore;
    private double sum = 0;
    Yan(String name, int ID,double mathScore,double englishScore) {
        this.name = name;
        this.ID = ID;
        this.mathScore=mathScore;
        this.englishScore=englishScore;
    }
    public double getMathScore() {
        return mathScore;
    }
    public double getEnglishScore() {
        return englishScore;
    }
    public double getSum() {
        return sum = (mathScore + englishScore);
    }
    public String getName() {
        return name;
    }
    public int getID() {
        return ID;
    }
}
class Key implements Comparable{
    double score=0;
    Key(double score) {
        this.score=score;
    }
    @Override
    public int compareTo(Object o) {
        Key key=(Key)o;
        if(this.score-((Key) o).score==0) {
            return -1;
        }
        else {
            return (int)(this.score-((Key) o).score)*1000;
        }
    }
}