/*
使用链表
 */


import java.util.*;

public class Dome01 {
    public static void main(String[] args) {
        LinkedList<Munber> list=new LinkedList<Munber>();
        String name[]={"飞机上","放假啊","撒盐含","大家说","的地方"};
        int ID[]={13,16,15,12,17};
        char sex[]={'男','男','女','男','男'};
        Munber munber[]=new Munber[5];
        for(int i=0;i<5;i++) {
            munber[i]=new Munber(name[i],ID[i],sex[i]);
            list.add(munber[i]);
        }
        Iterator<Munber> iter=list.iterator();
        System.out.println("排序前");
        while(iter.hasNext()) {
            Munber m=iter.next();
            System.out.println(m.getName()+" "+m.getID()+" "+m.getSex());
        }
        Collections.sort(list) ;
        System.out.println("排序后");
        iter=list.iterator();
        while (iter.hasNext()) {
            Munber m=iter.next();
            System.out.println(m.getName()+" "+m.getID()+" "+m.getSex());
        }
        System.out.println("插入");
        Munber munber1=new Munber("啦啦啦",14,'女');
        list.add(2,munber1);
        iter=list.iterator();
        while (iter.hasNext()) {
            Munber munber2=iter.next();
            System.out.println(munber2.getName()+" "+munber2.getID()+" "+munber2.getSex());
        }
    }
}
class Munber implements Comparable {
    private String name;
    private int ID;
    private char sex;
    private double mathScore;
    private double englishScore;
    private double sum = 0;
    @Override
    public int compareTo(Object o) {
        Munber m=(Munber)o;
        return (this.getID()-m.getID());
    }
    Munber(String name, int ID, char sex) {
        this.name = name;
        this.ID = ID;
        this.sex = sex;
    }
    Munber(String name, int ID, char sex,double mathScore,double englishScore) {
        this.name = name;
        this.ID = ID;
        this.sex = sex;
        this.mathScore=mathScore;
        this.englishScore=englishScore;
    }
    public void setMathScore(double score) {
        mathScore = score;
    }
    public double getMathScore() {
        return mathScore;
    }
    public void setEnglishScore(double score) {
        englishScore = score;
    }
    public double getEnglishScore() {
        return englishScore;
    }
    public double getSum() {
        return sum = (mathScore + englishScore) / 2;
    }
    public String getName() {
        return name;
    }
    public int getID() {
        return ID;
    }
    public char getSex() {
        return sex;
    }
}


