import java.util.*;
class Student1 implements Comparable {
   int height=0;
   String name;
   Student1(String n,int h) {
      name=n;
      height=h;

   }
   @Override
   public int compareTo(Object b) {
      Student1 st=(Student1)b;
      return (this.height-st.height);
   }
}
public class Example15_4 {
    public static void main(String[] args) {
        List<Student1> list=new LinkedList<Student1>();
        list.add(new Student1("张三",188));
        list.add(new Student1("李四",178));
        list.add(new Student1("周五",198));
        list.add(new Student1("李东骏",180));
        list.add(new Student1("陈卓",160));
        list.add(new Student1("杨靖涛",170));
        Iterator<Student1> iter=list.iterator();
        System.out.println("排序前，链表中的数据");
        while(iter.hasNext()) {
            Student1 stu=iter.next();
            System.out.println(stu.name+"的身高："+stu.height);
        }
        Collections.sort(list);
        System.out.println("排序后，链表中的数据为：");
        iter=list.iterator();
        while (iter.hasNext()) {
            Student1 stu=iter.next();
            System.out.println(stu.name+"的身高："+stu.height);
        }
        Student1 zhaoLin =new Student1("zhao xiao lin",178);
        int index=Collections.binarySearch(list,zhaoLin,null);
        if (index>=0) {
            System.out.println(zhaoLin.name+"和链表中的"+list.get(index).name+"身高一样");
        }
    }

}