import java.util.*;
class Student2 implements Comparable {
   int english=0;
   String name;
   Student2(int english,String name) {
      this.name=name;
      this.english=english;
   }
   public int compareTo(Object b) {
      Student2 st=(Student2) b;
      return (this.english-st.english);
   }
}
public class Example15_8 {
  public static void main(String args[]) {
     TreeSet<Student2> mytree=new TreeSet<Student2>();
     Student2 st1,st2,st3,st4;
     st1=new Student2(90,"赵一");
     st2=new Student2(66,"钱二");
     st3=new Student2(86,"孙三");
     st4=new Student2(76,"李四");
     mytree.add(st1);
     mytree.add(st2);
     mytree.add(st3);
     mytree.add(st4);
     Iterator<Student2> te=mytree.iterator();
     while(te.hasNext()) {
        Student2 stu=te.next();
        System.out.println(""+stu.name+" "+stu.english);
     }
  }
}
