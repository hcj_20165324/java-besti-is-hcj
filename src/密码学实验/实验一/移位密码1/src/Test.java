/**
 * Created by 81052 on 2018/4/8.
 */
import java.util.Scanner;

/**
 * 移位位数为左移位数
 * Created by 81052 on 2018/4/8.
 */


import java.util.Scanner;
public class Test {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入需要加密的明文：");
        String sourceString=sc.nextLine();
        System.out.println("请输入需要移位的位数：");
        int d=sc.nextInt();
        EncryptAndDecrypt person=new EncryptAndDecrypt();
        String secret=person.encrypt(sourceString,d);
        System.out.println("密文:"+secret);
        /*System.out.println("输入需要解密的密文:");
        secret=sc.nextLine();
        String source=person.decrypt(secret);
        System.out.println("明文:"+source);*/
    }
}
class EncryptAndDecrypt {
    String encrypt(String sourceString,int d) {
        char []p=sourceString.toCharArray();
        int n=p.length;
        int j=0;
        char []h=new char[n];
        for (int k=0;k<n;k++) {
            j=(k+d)%(n);
            h[k]=p[j];
        }
        return  new String(h);
    }
    String decrypt(String sourceString) {
        char []p=sourceString.toCharArray();
        int n=p.length;
        for (int k=0;k<n/2;k++) {
            char temp=p[n-k-1];
            p[n-k-1]=p[k];
            p[k]=temp;
        }
        return new String(p);
    }
}
