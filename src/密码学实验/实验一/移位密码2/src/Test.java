
/**
 * Created by 81052 on 2018/4/8.
 */
import java.util.Scanner;
public class Test {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入需要加密的明文：");
        String sourceString=sc.nextLine();
        System.out.println("请输入需要移位密钥：");
        String password=sc.nextLine();
        EncryptAndDecrypt person=new EncryptAndDecrypt();
        String secret=person.encrypt(sourceString,password);
        System.out.println("密文:"+secret);
        /*System.out.println("输入需要解密的密文:");
        secret=sc.nextLine();
        String source=person.decrypt(secret);
        System.out.println("明文:"+source);*/
    }
}
class EncryptAndDecrypt {
    String encrypt(String sourceString,String password) {
        char []s=sourceString.toCharArray();
        int m=s.length;
        char []p=password.toCharArray();
        int n=p.length;
        char []r=new char[m];
        for (int i=0;i<n;i++) {
            char t=s[0];
            char f=p[i];
            for (int j=0;j<f;j++) {
                s[j]=s[j+1];
                s[f]=t;
            }
        }
        return  new String(s);
    }
    String decrypt(String sourceString) {
        char []p=sourceString.toCharArray();
        int n=p.length;
        for (int k=0;k<n/2;k++) {
            char temp=p[n-k-1];
            p[n-k-1]=p[k];
            p[k]=temp;
        }
        return new String(p);
    }
}

