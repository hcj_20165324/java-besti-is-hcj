/*
实现线性反馈移位寄存器与对偶反馈移位寄存器。9位
 */
public class Test {
    public static void main(String[] args) {
        int  a[]={1,0,0,1,0,0,0,1,1,0};//初态
        int size=a.length;
        int k=-1;//输出序列元素
        for(int i=0;i<1023;i++) {
            operate(a,size);
            k=a[0];
            System.out.printf("%d",k);
        }
    }
    private static int[] operate(int [] a, int size) {
        int m=a[2]^a[9];//10阶的本原多项式为：1+x^3+x^10
        for(int j=0;j<size-1;j++) {
            a[j]=a[j+1];
        }
        a[9]=m;
        return a;
    }
}
