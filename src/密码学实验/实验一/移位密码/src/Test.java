/**
 * Created by 81052 on 2018/4/8.
 */

import java.util.Scanner;
public class Test {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入需要加密的明文：");
        String sourceString=sc.nextLine();
        EncryptAndDecrypt person=new EncryptAndDecrypt();
        String secret=person.encrypt(sourceString);
        System.out.println("密文:"+secret);
        System.out.println("输入需要解密的密文:");
        secret=sc.nextLine();
        String source=person.decrypt(secret);
        System.out.println("明文:"+source);
    }
}
class EncryptAndDecrypt {
    String encrypt(String sourceString) {
        char []p=sourceString.toCharArray();
        int n=p.length;
        for (int k=0;k<n/2;k+=2) {
            char temp=p[n-k-1];
            p[n-k-1]=p[k];
            p[k]=temp;
        }
        return  new String(p);
    }
    String decrypt(String sourceString) {
        char []p=sourceString.toCharArray();
        int n=p.length;
        for (int k=0;k<n/2;k+=2) {
            char temp=p[n-k-1];
            p[n-k-1]=p[k];
            p[k]=temp;
        }
        return new String(p);
    }
}