import java.util.Scanner;
public class Test {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入需要加密的明文：");
        String sourceString=sc.nextLine();
        EncryptAndDecrypt person=new EncryptAndDecrypt();
        System.out.println("输入密码加密:");
        String password=sc.nextLine();
        String secret=person.encrypt(sourceString,password);
        System.out.println("密文:"+secret);
        System.out.println("输入解密密码:");
        password=sc.nextLine();
        String source=person.decrypt(secret,password);
        System.out.println("明文:"+source);
    }
}
class EncryptAndDecrypt {
    String encrypt(String sourceString,String password) {
        char []p=password.toCharArray();
        int n=p.length;
        char []c=sourceString.toCharArray();
        int m=c.length;
        for (int k=0;k<m;k++) {
            int mima=c[k]+p[k%n];
            c[k]=(char)mima;
        }
        return  new String(c);
    }
    String decrypt(String sourceString,String password) {
        char []p=password.toCharArray();
        int n=p.length;
        char []c=sourceString.toCharArray();
        int m=c.length;
        for (int k=0;k<m;k++) {
            int mima=c[k]-p[k%n];
            c[k]=(char)mima;
        }
        return new String(c);
    }
}