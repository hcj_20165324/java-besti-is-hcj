public class Test1 {
    public static void main(String[] args) {
        int n = 16;//4级产生一个周期最多为16个元素的序列
        int[] a = {1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0};//4级产生32位，其最大周期为15
        int zhouqi = operate(a,n);
        System.out.println(zhouqi);
    }
    private static int operate(int[] a, int n) {
        int[] p = new int[n];
        int[] q = new int[n];
        boolean is=true;
        int i;
        for (i = 1; i <= n; i++) {
            for(int j=0;j<n;j++) {
                p[j] = a[j];
                q[j+i] = a[j+i];
                if (p[j] != q[2*j]) {
                    is=false;
                    break;
                }
            }

        }
        if(is) {
            return i;
        }
        else {
            return 0;
        }
    }
}