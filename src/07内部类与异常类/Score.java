/**
 * Created by 81052 on 2018/4/5.
 */
public class Score {
    int sum=0;
    public void In(int n) throws InputException {
        if(n<0) {
            throw new InputException(n);
        }
        sum+=n;
    }
    public int getSumScore() {
        return sum;
    }
}
