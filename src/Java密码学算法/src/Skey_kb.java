/*
将密钥通过文件流方式保存在文件中，在文件中保存的是字节。
 */
import java.io.*;
import java.security.*;
public class Skey_kb{
    public static void main(String args[]) throws Exception{
        FileInputStream f=new FileInputStream("key1.dat");//指定文件输入流
        ObjectInputStream b=new ObjectInputStream(f);//将文件输入流f作为参数传递给对象输入流
        Key k=(Key)b.readObject( );//执行对象输入流的readObject()方法读取密钥对象。由于该方法返回的是Objecj类型，需要强制转换为key类型
        byte[ ] kb=k.getEncoded( );//获取主要编码格式，在执行SecretKey类型的对象k的getEncoded()方法，返回的编码放在byte类型的数组中
        FileOutputStream  f2=new FileOutputStream("keykb1.dat");//指定文件输出流
        f2.write(kb);//保存密钥编码
        // 打印密钥编码中的内容
        for(int i=0;i<kb.length;i++){
            System.out.print(kb[i]+",");
        }
    }
}