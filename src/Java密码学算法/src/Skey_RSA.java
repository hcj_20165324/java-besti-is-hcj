/*
Java非对称加密-RSA算法
Java的KeyPairGenerator类提供了一些方法来创建密钥对以便用于非对称加密，密钥对创建好后封装在KeyPair类型的对象中，在KeyPair类中提供了获取公钥和私钥的方法。具体步骤如下：

（1） 创建密钥对生成器
KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
分析：密钥对生成器即KeyPairGenerator类型的对象，和2.2.1小节的第1步中介绍的KeyGenerator类一样，KeyPairGenerator类是一个工厂类，它通过其中预定义的一个静态方法getInstance（ ）获取KeyPairGenerator类型的对象。getInstance（ ）方法的参数是一个字符串，指定非对称加密所使用的算法，常用的有RSA，DSA等。

（2） 初始化密钥生成器
kpg.initialize(1024);
分析：对于密钥长度。对于RSA算法，这里指定的其实是RSA算法中所用的模的位数。可以在512到2048之间。

（3） 生成密钥对
KeyPair kp=kpg.genKeyPair( );
分析：

（4） 获取公钥和私钥
PublicKey pbkey=kp.getPublic( );
PrivateKey prkey=kp.getPrivate( );
分析：
 */
import java.io.*;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;

public class Skey_RSA{
    public static void main(String args[]) throws Exception{
        //KeyPairGenerator类提供了一些方法来创建密钥对以便用于非对称加密，密钥对创建好后封装在KeyPair类型的对象中
        KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);//初始化密钥生成器，指定的其实是RSA算法中所用的模的位数。可以在512到2048之间。
        KeyPair kp=kpg.genKeyPair();//使用KeyPairGenerator类的genKeyPair( )方法生成密钥对，其中包含了一对公钥和私钥的信息。
        PublicKey pbkey=kp.getPublic();//使用KeyPair类的getPublic( )和getPrivate( )方法获得公钥和私钥对象。
        PrivateKey prkey=kp.getPrivate();
        //  保存公钥        
        FileOutputStream  f1=new FileOutputStream("Skey_RSA_pub.dat");
        ObjectOutputStream b1=new  ObjectOutputStream(f1);
        b1.writeObject(pbkey);
        //  保存私钥
        FileOutputStream  f2=new FileOutputStream("Skey_RSA_priv.dat");
        ObjectOutputStream b2=new  ObjectOutputStream(f2);
        b2.writeObject(prkey);
    }
}