/*
（1） 从文件中获取密钥
（2） 创建密码器（Cipher对象）
（3） 初始化密码器
（4） 获取等待加密的明文
（5） 执行加密
（6） 处理加密结果
 */

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
public class Test1 {
    public static void main(String[] args)
            throws Exception {
        String s="Hello World";
        KeyGenerator kg=KeyGenerator.getInstance("DESede");
        kg.init(168);
        SecretKey k=kg.generateKey();
        FileOutputStream file=new FileOutputStream("yy.dat");
        ObjectOutputStream b=new ObjectOutputStream(file);
        b.writeObject(k);
        FileInputStream f=new FileInputStream("yy.dat");
        ObjectInputStream bb=new ObjectInputStream(f);
        Key key=(Key)bb.readObject();
        Cipher cp=Cipher.getInstance("DESede");
        cp.init(Cipher.ENCRYPT_MODE,key);
        byte ptext[]=s.getBytes("UTF8");
        for(int i=0;i<ptext.length;i++) {
            System.out.print(ptext[i]+",");
        }
        System.out.println(" ");
        byte ctext[]=cp.doFinal(ptext);
        for(int i=0;i<ctext.length;i++) {
            System.out.print(ctext[i]+",");
        }
        FileOutputStream f2=new FileOutputStream("yy.dat");
        f2.write(ctext);
    }
}
