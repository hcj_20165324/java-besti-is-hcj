public class Dome04 {
	public static void main(String[] args) {
		int []a={1,2,3,4};
		int []b={100,200,300};
		System.out.println("数组a中元素个数为:"+a.length);
		System.out.println("数组b中元素个数为:"+b.length);
		System.out.println("数组a的引用为:"+a);
		System.out.println("数组b的引用为:"+b);
        a=b;
		System.out.println("数组a中元素个数为:"+a.length);
		System.out.println("数组b中元素个数为:"+b.length);
        System.out.println("a[0]="+a[0]+",a[1]="+a[1]+",a[2]="+a[2]);
		System.out.println("b[0]="+b[0]+",b[1]="+b[1]+",b[2]="+b[2]);
		}
}
