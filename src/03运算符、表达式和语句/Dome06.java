//计算1+1/2！+1/3！+1/4!+...的前二十项和
public class Dome06{
	public static void main(String args[]){
		double sum=0,item=1;
		int i=1,n=20;
		while (i<n){
			sum=sum+item;
			i++;
			item=item*(1.0/i);
		}
		System.out.println("sum="+sum);
     }
}