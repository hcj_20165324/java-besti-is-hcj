/**
 * Created by 81052 on 2018/4/2.
 */
/*实现学生成绩管理功能（增删改，排序，查找），并进行测试（正常情况，异常情况，边界情况）

我觉得这道题目较难，设计程序花费了较长的时间，不过整体思路清晰，代码实现也是花费了很长的时间。
设计思路:这道题目我分为三个类来写
MainTest类为主类
Student类包含学生的具体信息
姓名
学号
成绩
java
math
English
总分
平均分
Operation类进行管理系统的操作
添加一个学生的信息
修改一个学生的信息
按照学号查找一个学生
对学生成绩进行排序
总分排序
平均分排序
单门课成绩排序*/
import java.util.*;
public class Test {
    public static void main(String[] args) {
        Student[] stu = new Student[100];
        Operate oper = new Operate();
        System.out.println("请选择你将进行的操作：（1为添加；2为修改；3为查找；4为排序;5为删除;6为显示;7为退出系统）");
        while(true) {
            Scanner sc = new Scanner(System.in);
            int n = sc.nextInt();
            if(n==1) {
                System.out.println("请按照以下格式输入学生信息：姓名 学号 性别 数学成绩 物理成绩：");
                oper.add(stu);
            }
            if (n==2 ){
                System.out.println("请输入需要修改的学生的学号:");
                int m=sc.nextInt();
                int j=oper.find(stu,m);
                System.out.println(j);
                if (j!=-1) {
                    oper.change(stu,j);
                }
                else {
                    System.out.println("无法修改学生信息");
                }
            }
            if (n==3) {
                System.out.println("请输入需要查找的学生的学号:");
                int m=sc.nextInt();
                int j=oper.find(stu,m);
            }
            if (n==4) {
                int m=oper.getLong(stu);
                oper.paixu(stu,m);
                System.out.println("按照学号升序排序完成");
            }
            if(n==6) {
                oper.Output(stu);
            }
            if(n==7) {
                System.out.println("程序已退出");
                System.exit(0);
            }
            if(n>7||n<1) {
                System.out.println("输入有误，请重新输入！");
            }
        }

    }
}
class Operate {
    public  void add (Student stu[]) {//输入为1
        Student stu1=new Student();
        Scanner s=new Scanner(System.in);
        stu1.name=s.next();
        stu1.number=s.nextInt();
        stu1.gender=s.next();
        stu1.mathScore=s.nextInt();
        stu1.phyScore=s.nextInt();
        int i=this.getLong(stu);
        stu[i]=stu1;
        System.out.println("学生信息录入完成");
    }
    public void change(Student stu[],int m) {//输入为2
        System.out.println("请输入修改后的成绩：");
        Scanner sc=new Scanner(System.in);
        stu[m].mathScore=sc.nextInt();
        stu[m].phyScore=sc.nextInt();
        System.out.println("修改后的学生信息为:");
        Output1(stu,m);
    }
    public int find(Student stu[],int number) {//输入为3
        Operate oper=new Operate();
        int i=this.getLong(stu);
        for (int j=0;j<i;j++) {
            if (number==stu[j].number) {
                System.out.println("该学生存在");
                oper.Output1(stu,j);
                return j;
            }
        }System.out.println("该学生不存在");
        return -1;
    }
    public void paixu(Student stu[],int n) {//输入为4
        for (int i=0;i<n-1;i++) {
            for (int j=i+1;j<n;j++) {
                if (stu[i].number>stu[j].number) {
                    Student s=stu[i];
                    stu[i]=stu[j];
                    stu[j]=s;
                }
            }
        }
    }
    public int getLong(Student[] stu){//学生个数
        for(int j=0;j<stu.length;j++){
            if(stu[j]==null){
                return j;
            }
        }
        return 10000;
    }
    public void Output(Student stu[]) {//输入为6
        int i=this.getLong(stu);
        for(int j=0;j<i;j++) {
            System.out.print("姓名:"+stu[j].name+"，学号:"+stu[j].number+"，性别:"+stu[j].gender+"，数学成绩:"+stu[j].mathScore);
            System.out.println("，物理成绩:"+stu[j].phyScore+"，总分:"+stu[j].getSumScore()+"，平均分为:"+stu[j].getAverScore());
        }
    }
    public void Output1(Student stu[],int j) {
        System.out.print("姓名:"+stu[j].name+"，学号:"+stu[j].number+"，性别:"+stu[j].gender+"，数学成绩:"+stu[j].mathScore);
        System.out.println("，物理成绩:"+stu[j].phyScore+"，总分:"+stu[j].getSumScore()+"，平均分为:"+stu[j].getAverScore());
    }
}
class Student {
    String name;
    int number;
    String gender;
    int mathScore;
    int phyScore;
    double getSumScore() {
        return mathScore+phyScore;
    }
    double averScore;
    double getAverScore() {
        return (mathScore+phyScore)/2;
    }
}
