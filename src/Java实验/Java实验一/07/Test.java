/*20165324 何春江 测试题第七题：实现求两个数的最大公约数和最小公倍数的功能，
数从命令行输入，并进行测试（正常情况，异常情况，边界情况）*/
public class Test {
	public static void main(String args[]) {
		int n=Integer.parseInt(args[0]);;
		int m=Integer.parseInt(args[1]);
		assert (n>0&&m>0):"非正数无最大公约数";
		int  max=gcd(n,m);
		int min=m*n/max;//最小公倍数为两个数之积除以最大公约数的商
		System.out.println(n+" "+m);
		System.out.println("两个数"+n+"和"+m+"的最大公约数为:"+max);
		System.out.println("两个数"+n+"和"+m+"的最小公倍数为:"+min);
	}
	static int gcd(int a,int b) {//辗转相除法
		while (a%b!=0) {
			int t=a%b;
			a=b;
			b=t;
		}
		return b;
	}
}
