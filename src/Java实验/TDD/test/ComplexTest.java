/*（1）属性：复数包含实部和虚部两个部分，
double RealPart;复数的实部
double ImagePart;复数的虚部
getRealPart():返回复数的实部
getImagePart();返回复数的虚部
setRealPart():设置复数的实部
setImagePart();设置复数的虚部
输出形式：a+bi
（2）方法：
①定义构造函数
public Complex()
public Complex(double R,double I)
②定义公有方法:加减乘除
Complex ComplexAdd(Complex a):实现复数加法
Complex ComplexSub(Complex a):实现复数减法
Complex ComplexMulti(Complex a):实现复数乘法
Complex ComplexDiv(Complex a):实现复数除法
③Override Object
public String toString():将计算结果转化为字符串形式并输出*/
import junit.framework.TestCase;
import org.testng.annotations.Test;

public class ComplexTest extends TestCase {
    Complex c1=new Complex(1,1);
    Complex c2=new Complex(1,0);
    Complex c3=new Complex(0,1);
    Complex c4=new Complex(-1,-1);
    @Test
    public void testComplexAdd() {
        assertEquals("2.0+1.0i",c1.ComplexAdd(c2).toString());
        assertEquals("1.0+1.0i",c2.ComplexAdd(c3).toString());
        assertEquals("1.0+1.0i",c3.ComplexAdd(c2).toString());
        assertEquals("0.0",c1.ComplexAdd(c4).toString());
    }
    @Test
    public void testComplexSub() {
        assertEquals("1.0i",c1.ComplexSub(c2).toString());
        assertEquals("1.0",c1.ComplexSub(c3).toString());
        assertEquals("2.0+2.0i",c1.ComplexSub(c4).toString());
        assertEquals("1.0-1.0i",c2.ComplexSub(c3).toString());
    }
    @Test
    public void testComplexMulti() {
        assertEquals("1.0+1.0i",c1.ComplexMulti(c2).toString());
        assertEquals("-1.0+1.0i",c1.ComplexMulti(c3).toString());
        assertEquals("1.0i",c2.ComplexMulti(c3).toString());
        assertEquals("-2.0i",c1.ComplexMulti(c4).toString());
    }
    @Test
    public void testComplexDiv() {
        assertEquals("1.0+1.0i", c1.ComplexDiv(c2).toString());
        assertEquals("1.0+1.0i", c1.ComplexDiv(c3).toString());
        assertEquals("-1.0-1.0i", c1.ComplexDiv(c4).toString());
        assertEquals("-0.5-0.5i", c2.ComplexDiv(c4).toString());
    }
}