import junit.framework.TestCase;
import org.testng.annotations.Test;
public class StringBufferDemoTest extends TestCase {
    StringBuffer a= new StringBuffer("String");
    StringBuffer b = new StringBuffer("StringBuffer");
    @Test
    public void testcharAt() throws Exception{
        assertEquals('S',a.charAt(0));
        assertEquals('g',a.charAt(5));
    }
    @Test
    public void testcapacity() throws Exception{
        assertEquals(22,a.capacity());
        assertEquals(28,b.capacity());
    }
    @Test
    public void testlength() throws Exception{
        assertEquals(6,a.length());
        assertEquals(12,b.length());
    }
    @Test
    public void testindexOf() throws Exception{
        assertEquals(0,a.indexOf("Str"));
        assertEquals(6,b.indexOf("Bu"));
    }
}