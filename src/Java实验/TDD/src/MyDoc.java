// Server Classes
abstract class Data {//抽象一个类
    abstract public void DisplayValue();//抽象方法，无返回。
}
class Integer extends  Data {//继承抽象类，重写方法
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class byteteger extends Data {
    byte value;
    byteteger() {
        value=24;
    }
    public void DisplayValue() {
        System.out.println(value);
    }
}
// Pattern Classes
abstract class Factory {//抽象一个类
    abstract public Data CreateDataObject();//抽象方法，返回一个Date类创建的对象
}
class IntFactory extends Factory {//继承抽象类，重写方法
    public Data CreateDataObject(){//返回一个Integer类创建的对象
        return new Integer();
    }
}
class byteFactoey extends Factory {
    public Data CreateDataObject() {
        return new byteteger();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    static Document b;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();
        b=new Document(new byteFactoey());
        b.DisplayData();
    }
}