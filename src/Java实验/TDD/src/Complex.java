/*（1）属性：复数包含实部和虚部两个部分，
double RealPart;复数的实部
double ImagePart;复数的虚部
getRealPart():返回复数的实部
getImagePart();返回复数的虚部
setRealPart():设置复数的实部
setImagePart();设置复数的虚部
输出形式：a+bi
（2）方法：
①定义构造函数
public Complex()
public Complex(double R,double I)
②定义公有方法:加减乘除
Complex ComplexAdd(Complex a):实现复数加法
Complex ComplexSub(Complex a):实现复数减法
Complex ComplexMulti(Complex a):实现复数乘法
Complex ComplexDiv(Complex a):实现复数除法
③Override Object
public String toString():将计算结果转化为字符串形式并输出
*/
public class Complex {
    private double RealPart;//复数的实部
    private double ImagePart;//复数的虚部
    public Complex() {}
    public Complex(double a, double b) {
        setRealPart(a);
        setImagePart(b);
    }
    public void setRealPart(double a) {
        RealPart = a;
    }
    public void setImagePart(double b) {
        ImagePart = b;
    }
    public double getRealPart() {//返回复数的实部
        return RealPart;
    }
    public double getImagePart() {
        return ImagePart;
    }
    Complex ComplexAdd(Complex a) {//(a+bi)+(c+di)=(a+c)+(b+d)i
        Complex complex = new Complex();
        complex.RealPart = this.RealPart + a.RealPart;
        complex.ImagePart = this.ImagePart + a.ImagePart;
        return complex;
    }
    Complex ComplexSub(Complex a) {//(a+bi)-(c+di)=(a-c)+(b-d)i
        Complex complex=new Complex();
        complex.RealPart=this.RealPart-a.RealPart;
        complex.ImagePart=this.ImagePart-a.ImagePart;
        return complex;
    }
    Complex ComplexMulti(Complex a) {//(a+bi)*(c+di)=(ac-bd)+(ad+bc)i
        Complex complex =new Complex();
        complex.RealPart=this.RealPart*a.RealPart-this.ImagePart*a.ImagePart;
        complex.ImagePart=this.RealPart*a.ImagePart+this.ImagePart*a.RealPart;
        return complex;
    }
    Complex ComplexDiv(Complex a) {//(a+bi)/(c+di)=(a+bi)(c-di)/(c^2+d^2)
        // return new Complex((r * c.i + i * c.r)/(c.i * c.i + c.r * c.r), (i * c.i + r * c.r)/(c.i * c.i + c.r * c.r));
        Complex complex=new Complex();
        complex.RealPart=(this.RealPart*a.ImagePart+this.ImagePart*a.RealPart)/(a.ImagePart*a.ImagePart+a.RealPart*a.RealPart);
        complex.ImagePart=(this.ImagePart*a.ImagePart+this.RealPart*a.RealPart)/(a.ImagePart*a.ImagePart+a.RealPart*a.RealPart);
        return complex;
    }
    public String toString() {
        if (ImagePart==0) {
            String str = String.valueOf(RealPart);
            return str;
        }
        else if (RealPart==0) {
            String str = String.valueOf(ImagePart)+"i";
            return  str;
        }
        else {
            if (ImagePart>0) {
                String str = String.valueOf(RealPart) + "+" + String.valueOf(ImagePart) + "i";
                return str;
            }
            else {
                String str = String.valueOf(RealPart) + String.valueOf(ImagePart) + "i";
                return str;
            }
        }
    }
}