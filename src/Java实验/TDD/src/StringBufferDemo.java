public class StringBufferDemo {
    public static void main(String [] args) {
        StringBuffer buffer = new StringBuffer();
        buffer.append('S');//调用方法append，输入char型
        buffer.append("tringBuffer");//调用方法append，输入String型
        System.out.println(buffer.charAt(1));//t
        System.out.println(buffer.capacity());//返回buffer对象的容量:16
        System.out.println(buffer.length());//返回buffer对象的长度:12
        System.out.println(buffer.indexOf("tring"));//返回输入的子字符串的第一个字母在母字符串的位置。
        System.out.println("buffer = " + buffer.toString());
    }
}
