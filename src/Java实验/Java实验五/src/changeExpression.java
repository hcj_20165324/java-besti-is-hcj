//yh666
import java.util.*;
public class changeExpression{
    String originalExpression;
    String changedExpression= "";
    int countLeft=0,countRight=0;
    public void setOriginalExpression(String str){
        originalExpression=str;
    }
    public String changedWay(){
        Stack stackChange=new Stack();//创立栈
        for (int i=0;i<originalExpression.length() ;i++) {
            char chi=originalExpression.charAt(i);
            if (chi>='0'&&chi<='9'){
                changedExpression=changedExpression+chi;
            }
            else if (chi=='+'||chi=='-'||chi=='*'||chi=='/') {
                changedExpression=changedExpression+" ";//有运算符，数字之间就要有空格，否则是一个整体
                if (stackChange.empty()){//栈为空直接压栈
                    stackChange.push(chi);
                }
                else if (judgeValue(chi)>=judgeValue((char)stackChange.peek())) {//运算级别高或者相等压入栈
                    stackChange.push(chi);
                }
                else{
                    changedExpression=changedExpression+ String.valueOf(stackChange.pop())+" ";//否则直接进入字符串,空格分割运算符
                    i--;
                }
            }
            else if(chi=='('){
                countLeft++;
                stackChange.push(chi);//左括号压栈
            }
            else if(chi==')'){
                changedExpression+=" ";
                countRight++;
                while((char)stackChange.peek()!='('){//直到（为止
                    changedExpression=changedExpression+ String.valueOf(stackChange.pop())+" ";//弹出栈内东西,空格分割
                }
                stackChange.pop();
            }
        }
        changedExpression+=" ";
        while(!stackChange.empty()){
            changedExpression=changedExpression+String.valueOf(stackChange.pop())+" ";
        }
        if (countLeft!=countRight) {
            System.out.println("括号不匹配");
            System.exit(0);
        }
        return changedExpression;
    }
    public int judgeValue(char c){
        int value=0;
        switch(c){
            case '(':
                value=1;
                break;
            case '+':
            case '-':
                value=2;
                break;
            case '*':
            case '/':
                value=3;
                break;
            case ')':
                value=4;
            default:
                value=0;
        }
        return value;
    }
}

