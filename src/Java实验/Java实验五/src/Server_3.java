import java.io.*;
import java.net.*;
import java.util.*;

public class Server_3 {
    public static void main(String[] args) {
        ServerSocket Server_2forClient_2=null;
        Socket SocketOnServer_2=null;
        DataOutputStream Server_2out=null;
        DataInputStream Server_2in=null;
        try {
            Server_2forClient_2=new ServerSocket(5300);
        }
        catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待客户端呼叫……");
            SocketOnServer_2=Server_2forClient_2.accept();
            Server_2out=new DataOutputStream(SocketOnServer_2.getOutputStream());
            Server_2in=new DataInputStream(SocketOnServer_2.getInputStream());
            String Ciphertext=Server_2in.readUTF();//密文
            byte[] data= Base64.getDecoder().decode(Ciphertext);
            String expr= AES.DecryptionAES(data);
            System.out.println("服务器接收到表达式："+expr);
            int result;
            MyBC mybc=new MyBC();
            result=mybc.evaluate(expr);
            Server_2out.writeUTF("后缀表达式:"+expr+"，运算结果为："+result);
            Thread.sleep(500);
        }
        catch (Exception e2) {
            System.out.println("客户端已断开"+e2);
        }
    }
}