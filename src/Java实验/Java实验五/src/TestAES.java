import sun.security.krb5.internal.crypto.Aes128;

import java.util.Scanner;

public class TestAES {
    public static void main(String[] args) {
        System.out.println("输入需要加密的字符串：");
        Scanner sc=new Scanner(System.in);
        String Plaintext=sc.nextLine();
        try {
            AES.produceAESKey();//生成AES密钥
            byte[] Key=AES.changeWay();//需要传输的密钥，数组形式传输。
            for (int i=0;i<Key.length;i++) {
                System.out.print(Key[i]+" ");
            }
            byte[]Ciphertext= AES.EncryptionAES(Plaintext);//需要传输的密文，数组形式传输。
            for (int i=0;i<Ciphertext.length;i++) {
                System.out.print(Ciphertext[i]+" ");
            }
            String s=AES.DecryptionAES(Ciphertext);//解密得到明文，String类型。
            System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
