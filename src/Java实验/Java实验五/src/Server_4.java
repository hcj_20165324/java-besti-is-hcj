

import java.io.*;
import java.net.*;
import java.util.*;

public class Server_4 {
    public static void main(String[] args) {
        ServerSocket Server_2forClient_2=null;
        Socket SocketOnServer_2=null;
        DataOutputStream Server_2out=null;
        DataInputStream Server_2in=null;
        try {
            Server_2forClient_2=new ServerSocket(5200);
        }
        catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待客户端呼叫……");
            SocketOnServer_2=Server_2forClient_2.accept();
            Server_2out=new DataOutputStream(SocketOnServer_2.getOutputStream());
            Server_2in=new DataInputStream(SocketOnServer_2.getInputStream());
            String Ciphertext=Server_2in.readUTF();//密文
            byte[] data= Base64.getDecoder().decode(Ciphertext);
            byte[] hh=AES_DH.DecryptionDES(data);
            Server_2out.writeUTF("接受到使用DH算法生成的共享密钥加密AES密钥生成的密文为：");
            for (int i=0;i<hh.length;i++) {
                System.out.print(hh[i]+" ");
            }
            Thread.sleep(500);
        }
        catch (Exception e2) {
            System.out.println("客户端已断开"+e2);
        }
    }
}