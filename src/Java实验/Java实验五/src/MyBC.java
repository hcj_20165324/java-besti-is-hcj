import java.util.Stack;
import java.util.StringTokenizer;

public class MyBC {
    /** constant for addition symbol */
    private final char ADD = '+';
    /** constant for subtraction symbol */
    private final char SUBTRACT = '-';
    /** constant for multiplication symbol */
    private final char MULTIPLY = '*';
    /** constant for division symbol */
    private final char DIVIDE = '/';
    /** the stack */
    Stack<Integer> stack=new Stack<Integer>();;

    String expression;
    public void setExpression(String str) {
        expression=str;
    }
    public  String changedWay() {
        String changedExpression = "";
        int countLeft = 0,countRight=0;
        Stack stackChange=new Stack();//创立栈
        for (int i=0;i<expression.length() ;i++) {
            char chi=expression.charAt(i);
            if (chi>='0'&&chi<='9'){
                changedExpression=changedExpression+chi;
            }
            else if (chi=='+'||chi=='-'||chi=='*'||chi=='/') {
                changedExpression=changedExpression+" ";//有运算符，数字之间就要有空格，否则是一个整体
                if (stackChange.empty()){//栈为空直接压栈
                    stackChange.push(chi);
                }
                else if (judgeValue(chi)>=judgeValue((char)stackChange.peek())) {//运算级别高或者相等压入栈
                    stackChange.push(chi);
                }
                else{
                    changedExpression=changedExpression+ String.valueOf(stackChange.pop())+" ";//否则直接进入字符串,空格分割运算符
                    i--;
                }
            }
            else if(chi=='('){
                countLeft++;
                stackChange.push(chi);//左括号压栈
            }
            else if(chi==')'){
                changedExpression+=" ";
                countRight++;
                while((char)stackChange.peek()!='('){//直到（为止
                    changedExpression=changedExpression+ String.valueOf(stackChange.pop())+" ";//弹出栈内东西,空格分割
                }
                stackChange.pop();
            }
        }
        changedExpression+=" ";
        while(!stackChange.empty()){
            changedExpression=changedExpression+String.valueOf(stackChange.pop())+" ";
        }
        if (countLeft!=countRight) {
            System.out.println("括号不匹配");
            System.exit(0);
        }
        return changedExpression;
    }

    private static int judgeValue(char c) {
        int value = 0;
        switch (c) {
            case '(':
                value = 1;
                break;
            case '+':
            case '-':
                value = 2;
                break;
            case '*':
            case '/':
                value = 3;
                break;
            case ')':
                value = 4;
            default:
                value = 0;
        }
        return value;
    }
    public int evaluate (String expr)
    {//后缀表达式的运算方法
        int op1, op2, result = 0;
        String token;
        StringTokenizer tokenizer = new StringTokenizer (expr);//使用StringTokenizer类分解String对象的字符序列，默认为空格符...
        //此时tokenizer为一个分析器
        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();
            if (isOperator(token))
            {
                op2 = (stack.pop()).intValue();//出栈
                op1 = (stack.pop()).intValue();//出栈
                result = evalSingleOp (token.charAt(0), op1, op2);//String对象第一个字符转换为char类型的方法为：str.charAt(0)
                stack.push (new Integer(result));//进栈
            }
            else {
                stack.push(new Integer(Integer.parseInt(token)));//进栈
            }
        }
        return result;
    }
    private boolean isOperator (String token)
    {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/") );
    }
    private int evalSingleOp (char operation, int op1, int op2)
    {
        int result = 0;
        switch (operation)
        {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
        }
        return result;
    }
}

