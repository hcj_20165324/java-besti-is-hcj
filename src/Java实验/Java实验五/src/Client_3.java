import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.*;

public class Client_3 {
    public static void main(String[] args) {
        Socket Client_2Socket;
        DataInputStream Client_2in=null;
        DataOutputStream Client_2out=null;
        String expr=null;
        String str=null;
        String Ciphertext=null;
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入表达式：");
        str=scanner.nextLine();
        MyBC mybc=new MyBC();
        mybc.setExpression(str);
        expr=mybc.changedWay();
        try {
            AES.produceAESKey();//生成AES密钥
            byte[]cc= AES.EncryptionAES(expr);//需要传输的密文，数组形式传输。
            Ciphertext = Base64.getEncoder().encodeToString(cc);//将加密后的密文由byte[]转换为String类型
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Client_2Socket=new Socket("172.30.2.248",5300);
            Client_2in=new DataInputStream(Client_2Socket.getInputStream());
            Client_2out=new DataOutputStream(Client_2Socket.getOutputStream());
            Client_2out.writeUTF(Ciphertext);
            String s=Client_2in.readUTF();
            System.out.println("服务器回复：\n"+s);
        }
        catch (Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}