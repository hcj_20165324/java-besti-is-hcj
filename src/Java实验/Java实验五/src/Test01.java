import java.util.Scanner;

public class Test01 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入运算式");
        String str=sc.nextLine();
        MyBC myBC=new MyBC();
        myBC.setExpression(str);
        String changedExpression=myBC.changedWay();
        System.out.println("中缀表达式转换为后缀表达式为:"+changedExpression);
        int result=myBC.evaluate(changedExpression);
        System.out.println("运算结果为:"+result);
    }
}
