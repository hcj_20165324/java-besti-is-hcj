import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.security.Key;

public class AES_DH {
    //AES加密，返回密文。
    public static byte[] EncryptionDES(byte[]data) throws Exception {
        FileInputStream f = new FileInputStream("A_Key_DESede_DH.dat");
        ObjectInputStream b = new ObjectInputStream(f);
        Key k = (Key) b.readObject();//从文件中获取密钥
        Cipher cp = Cipher.getInstance("AES");//创建密码器
        cp.init(Cipher.ENCRYPT_MODE, k);//初始化密码器，第一个参数指定密码器准备进行加密还是解密，第二个参数则传入加密或解密所使用的密钥。
        byte ctext[] = cp.doFinal(data);//执行Cipher对象的doFinal()方法，该方法的参数中传入待加密的明文，从而按照前面设置的算法与模式对明文加密。
        System.out.println("共享密钥加密完成！");
        //FileOutputStream f2 = new FileOutputStream("Ciphertext.txt");//将密文保存于Ciphertext.dat中。
        //f2.write(ctext);//处理加密结果
        return ctext;
    }
    //DESede解密，信息保存。
    public static byte[] DecryptionDES(byte[]data) throws Exception{
        FileInputStream f = new FileInputStream("A_Key_DESede_DH.dat");
        ObjectInputStream b = new ObjectInputStream(f);
        Key k = (Key) b.readObject();//从文件中获取密钥
        Cipher cp = Cipher.getInstance("AES");//创建密码器
        cp.init(Cipher.DECRYPT_MODE,k);
        byte ctext[]=cp.doFinal(data);
        //FileOutputStream ff=new FileOutputStream("Plaintext.txt");//将明文保存于Plaintext.dat中。
        //ff.write(ctext);
        System.out.println("共享密钥解密完成！");
        return ctext;
    }
}
