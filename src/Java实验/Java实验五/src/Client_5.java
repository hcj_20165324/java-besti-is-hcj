import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.*;

public class Client_5 {
    public static void main(String[] args) {
        Socket Client_2Socket;
        DataInputStream Client_2in=null;
        DataOutputStream Client_2out=null;
        String expr,str;
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入表达式：");
        str=scanner.nextLine();
        MyBC mybc=new MyBC();
        mybc.setExpression(str);
        expr=mybc.changedWay();
        try {
            MessageDigest m=MessageDigest.getInstance("MD5");//生成MessageDigest对象,使用静态方法
            m.update(expr.getBytes("UTF8"));//x为需要计算的字符串，update传入的参数是字节类型或字节类型数组，对于字符串，需要先使用getBytes( )方法生成字符串数组。
            byte ss[ ]=m.digest( );//计算的结果通过字节类型的数组返回。
            String result="";
            for (int i=0; i<ss.length; i++){
                //处理计算结果
                result+=Integer.toHexString((0x000000ff & ss[i]) |
                        0xffffff00).substring(6);
            }
            Client_2Socket=new Socket("172.30.2.248",5555);
            Client_2in=new DataInputStream(Client_2Socket.getInputStream());
            Client_2out=new DataOutputStream(Client_2Socket.getOutputStream());
            Client_2out.writeUTF(result);
            String s=Client_2in.readUTF();
            System.out.println("服务器回复：\n"+s);
        }
        catch (Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}