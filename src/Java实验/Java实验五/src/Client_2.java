import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.*;

public class Client_2 {
    public static void main(String[] args) {
        Socket Client_2Socket;
        DataInputStream Client_2in=null;
        DataOutputStream Client_2out=null;
        String expr,str;
        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入表达式：");
        str=scanner.nextLine();
        MyBC mybc=new MyBC();
        mybc.setExpression(str);
        expr=mybc.changedWay();
        try {
            Client_2Socket=new Socket("192.168.43.77",5353);
            Client_2in=new DataInputStream(Client_2Socket.getInputStream());
            Client_2out=new DataOutputStream(Client_2Socket.getOutputStream());
            Client_2out.writeUTF(expr);
            String s=Client_2in.readUTF();
            System.out.println("服务器回复：\n"+s);
        }
        catch (Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}