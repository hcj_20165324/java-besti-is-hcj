import java.io.*;
public class Read implements Runnable {
    DataInputStream in;
    public void setDataInputStream(DataInputStream in) {
        this.in = in;
    }
    public void run() {
        String expression=null;
        while(true) {
            try{ expression= String.valueOf(in.read());
                System.out.println("表达式值："+expression);
                System.out.println("中缀表达式:（放弃请输入N）");
            }
            catch(IOException e) {
                System.out.println("与服务器已断开"+e);
                break;
            }
        }
    }
}