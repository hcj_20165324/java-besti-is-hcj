package hcj;
import java.util.Scanner;
public class Test {
    public static void main(String args[]) {
        System.out.println("输入两个班的人数:");
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int m=sc.nextInt();
        Student []stu1=new Student[n];//创建对象数组
        Student []stu2=new Student[m];//创建对象数组
        Fen(stu1,n);
        Fen(stu2,m);
        System.out.println("现在输入一班学生信息：");
        Input(stu1,n);
        System.out.println("现在输入二班学生信息：");
        Input(stu2,m);
        paixu(stu1,n);
        paixu(stu2,m);
        System.out.println("一班信息为：");
        Output(stu1,n);
        System.out.println("二班信息为：");
        Output(stu2,m);
    }
    private static void Fen(Student[] stu, int n) {//分配空间
        for (int i=0;i<n;i++) {
            stu[i]=new Student();
        }
    }

    private static void Input(Student stu[],int n) {//输入
        Scanner sc=new Scanner(System.in);
        for (int i=0;i<n;i++) {
            System.out.println("请按以下格式输入学生信息：姓名 学号 性别 数学成绩 物理成绩");
            stu[i].setName(sc.next());
            stu[i].setNumber(sc.nextInt());
            stu[i].setGender(sc.next());
            stu[i].setMathScore(sc.nextInt());
            stu[i].setPhyScore(sc.nextInt());
        }
    }
    private static void paixu(Student stu[],int n) {
        for (int i=0;i<n-1;i++) {
            for (int j=i+1;j<n;j++) {
                if (stu[i].getNumber() > stu[j].getNumber()) {
                    Student s=stu[i];
                    stu[i]=stu[j];
                    stu[j]=s;
                }
            }
        }
    }
    private static  void Output(Student stu[],int n) {
        for (int i=0;i<n;i++) {
            System.out.println("姓名："+ stu[i].getName() +"，学号："+ stu[i].getNumber() +"，性别："+ stu[i].getGender() +"，数学成绩："+ stu[i].getMathScore() +"，物理成绩："+ stu[i].getPhyScore());
        }
    }
}
class Student {
    private String name;
    private int number;
    private String gender;
    private int mathScore;
    private int phyScore;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getMathScore() {
        return mathScore;
    }

    public void setMathScore(int mathScore) {
        this.mathScore = mathScore;
    }

    public int getPhyScore() {
        return phyScore;
    }

    public void setPhyScore(int phyScore) {
        this.phyScore = phyScore;
    }
}
