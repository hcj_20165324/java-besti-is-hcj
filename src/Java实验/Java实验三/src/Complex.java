public class Complex {
    private double r;
    private double i;
    public void SetReal(double real) {
        r=real;
    }
    public void SetImage(double image) {
        i=image;
    }
    public double GetReal() {
        return r;
    }
    public double GetImage() {
        return i;
    }
    public Complex() {

    }
    public Complex(double real,double image) {
        SetReal(real);
        SetImage(image);
    }
    Complex ComplexAdd(Complex a) {
        Complex c=new Complex();
        c.r=this.r+a.r;
        c.i=this.i+a.i;
        return c;
    }
    Complex ComplexSub(Complex a) {
        Complex c=new Complex();
        c.r=this.r-a.r;
        c.i=this.i-a.i;
        return c;
    }
    Complex ComplexMulti(Complex a) {
        Complex c=new Complex();
        c.r=this.r*a.r-this.i*a.i;
        c.i=this.r*a.i+this.i*a.r;
        return c;
    }
    Complex ComplexDiv(Complex a) {
        Complex c=new Complex();
        double d=a.r*a.r+a.i*a.i;
        c.r=(this.r*a.r+this.i*a.i)/d;
        c.i=(this.i*a.r-this.r*a.i)/d;
        return c;
    }
    public String toString() {
        String s="0";
        if(i==0) {
            s=String.valueOf(r);
            return s;
        }
        else if(r==0) {
            s=String.valueOf(i)+"i";
            return s;
        }
        else {
            if(i>0) {
                s=String.valueOf(r)+"+"+String.valueOf(i)+"i";
                return s;
            }
            else if(i<0) {
                s=String.valueOf(r)+String.valueOf(i)+"i";
                return s;
            }
        }
        return s;
    }
}