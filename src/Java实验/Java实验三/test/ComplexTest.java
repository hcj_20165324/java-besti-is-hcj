import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a=new Complex(2.0,1.0);
    Complex b=new Complex(-5.0,3.0);
    Complex c=new Complex(4.0,-2.0);
    @Test
    public void testComplexAdd() {
        assertEquals("-3.0+4.0i",a.ComplexAdd(b).toString());
        assertEquals("6.0-1.0i",c.ComplexAdd(a).toString());
    }
    @Test
    public void testComplexSub() {
        assertEquals("-2.0+3.0i",a.ComplexSub(c).toString());
        assertEquals("7.0-2.0i",a.ComplexSub(b).toString());
    }
    @Test
    public void testComplexMulti() {
        assertEquals("-13.0+1.0i",a.ComplexMulti(b).toString());
        assertEquals("10.0",a.ComplexMulti(c).toString());
    }

}