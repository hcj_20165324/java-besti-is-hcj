import java.util.StringTokenizer;
import java.util.Stack;

public class MyDcRational
{
    /** constant for addition symbol */
    private final char ADD = '+';
    /** constant for subtraction symbol */
    private final char SUBTRACT = '-';
    /** constant for multiplication symbol */
    private final char MULTIPLY = '*';
    /** constant for division symbol */
    private final char DIVIDE = '/';
    /** the stack */
    private Stack stack;//存放操作数的栈
    public MyDcRational()
    {
        stack = new Stack();
    }

    public Rational evaluate (String expr)
    {
        Rational op1=new Rational();
        Rational op2=new Rational();
        Rational result=new Rational();
        result.setNumerator(0);
        String token;
        StringTokenizer tokenizer = new StringTokenizer (expr);//划分表达式

        while (tokenizer.hasMoreTokens())
        {
            token = tokenizer.nextToken();//将算数表达式分解的

            if (isOperator(token))//见下方isOperateor方法,当是运算符的时候进入if语句
            {
                op2 = (Rational) stack.pop();
                op1 = (Rational)stack.pop();//弹出最上面两个操作数
                result = evalSingleOp (token.charAt(0), op1, op2);//见下方evaSingleOp方法
                stack.push (result);//将计算结果压栈
            }
            else{
                Rational num=new Rational();
                num.setNumerator(Integer.parseInt(token));//将操作数由string转变为Rational
                stack.push (num);//操作数入栈
            }

        }

        return result;//输出结果
    }

    private boolean isOperator (String token)//判断是否为运算符,注意用equal语句比较字符串
    {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/") );
    }

    private Rational evalSingleOp (char operation, Rational op1, Rational op2)
    {
        Rational result=new Rational();
        result.setNumerator(0);
        switch (operation)
        {
            case ADD:
                result = op1.add(op2);
                break;
            case SUBTRACT:
                result = op1.sub(op2);
                break;
            case MULTIPLY:
                result = op1.muti(op2);
                break;
            case DIVIDE:
                result = op1.div(op2);
                break;
            default:
                System.out.println("Error!");
        }
        return result;
    }
}