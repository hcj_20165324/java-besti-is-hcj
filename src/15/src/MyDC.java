import java.util.StringTokenizer;
import java.util.Stack;

public class MyDC
{
    /** constant for addition symbol */
    private final char ADD = '+';
    /** constant for subtraction symbol */
    private final char SUBTRACT = '-';
    /** constant for multiplication symbol */
    private final char MULTIPLY = '*';
    /** constant for division symbol */
    private final char DIVIDE = '/';
    /** the stack */
    private Stack<Integer> stack;//存放操作数的栈,且只能存放Integer型
    public MyDC()
    {
        stack = new Stack<Integer>();
    }

    public int evaluate (String expr)
    {
        int op1, op2, result = 0;
        String token;
        StringTokenizer tokenizer = new StringTokenizer (expr);//划分表达式

        while (tokenizer.hasMoreTokens())
        {
            token = tokenizer.nextToken();//将算数表达式以空格为分隔符进行分解

            if (isOperator(token))//见下方isOperateor方法,当是运算符的时候进入if语句
            {
                op2 = (stack.pop()).intValue();
                op1 = (stack.pop()).intValue();//弹出最上面两个操作数
                result = evalSingleOp (token.charAt(0), op1, op2);//见下方evaSingleOp方法
                stack.push (new Integer(result));//将计算结果压栈
            }
            else
                stack.push (new Integer(Integer.parseInt(token)));//操作数入栈
        }

        return result;//输出结果
    }

    private boolean isOperator (String token)//判断是否为运算符,注意用equal语句比较字符串
    {
        return ( token.equals("+") || token.equals("-") ||
                token.equals("*") || token.equals("/") );
    }

    private int evalSingleOp (char operation, int op1, int op2)
    {
        int result = 0;

        switch (operation)
        {
            case ADD:
                result = op1 + op2;
                break;
            case SUBTRACT:
                result = op1 - op2;
                break;
            case MULTIPLY:
                result = op1 * op2;
                break;
            case DIVIDE:
                result = op1 / op2;
        }

        return result;
    }
}