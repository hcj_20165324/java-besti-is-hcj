import java.util.*;
class Student1 implements Comparable {
    int ID=0;
    String name;
    Student1(String n,int h) {
        name=n;
        ID=h;

    }
    @Override
    public int compareTo(Object b) {
        Student1 st=(Student1)b;
        return (this.ID-st.ID);
    }
}
public class Mylist {
    public static void main(String[] args) {
        List<Student1> list=new LinkedList<Student1>();
        list.add(new Student1("王瑶佳",22));
        list.add(new Student1("杨金川",23));
        list.add(new Student1("李东骏",25));
        list.add(new Student1("陈卓",26));

        Iterator<Student1> iter=list.iterator();
        System.out.println("插入前，链表中的数据");
        while(iter.hasNext()) {
            Student1 stu=iter.next();
            System.out.println(stu.name+"的学号："+stu.ID);
        }
        System.out.println("插入后，遍历结点，链表中的数据为：");
        list.add(2,new Student1("何春江",24));
        iter=list.iterator();
        while (iter.hasNext()) {
            Student1 stu=iter.next();
            System.out.println(stu.name+"的学号："+stu.ID);
        }
        System.out.println("删除后，遍历结点，链表中的数据为：");
        list.remove(2);
        iter=list.iterator();
        while (iter.hasNext()) {
            Student1 stu=iter.next();
            System.out.println(stu.name+"的学号："+stu.ID);
        }
        }
}

