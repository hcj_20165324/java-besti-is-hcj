import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.*;
import java.math.BigInteger;
import java.security.Key;
import java.util.Scanner;

public class Test01 {
    //密钥的生成及保存。
    public static void produceAESKey() throws Exception {
        KeyGenerator kg = KeyGenerator.getInstance("AES");//1.获取密钥生成器
        kg.init(128);//初始化密钥生成器，AES算法密钥长度为128位
        SecretKey k = kg.generateKey();//生成密钥
        byte[ ] kb=k.getEncoded( );
        BigInteger bigInteger=new BigInteger(kb);
        FileOutputStream f = new FileOutputStream("AESkey.dat");//指定产生密钥输出流文件
        ObjectOutputStream b = new ObjectOutputStream(f);//将对象序列化，以流的方式进行处理
        b.writeObject(k);//通过以对象序列化方式将密钥保存在文件中
        System.out.println("AES密钥生成完成");
    }

    //将AES的密钥转换为字节码文件,发送至对象
    public static byte[] changeWay ()throws Exception {
        FileInputStream f = new FileInputStream("AESkey.dat");//指定文件输入流
        ObjectInputStream b = new ObjectInputStream(f);//将文件输入流f作为参数传递给对象输入流
        Key k = (Key) b.readObject();//执行对象输入流的readObject()方法读取密钥对象。由于该方法返回的是Objecj类型，需要强制转换为key类型
        byte[] kb = k.getEncoded();//获取主要编码格式，在执行SecretKey类型的对象k的getEncoded()方法，返回的编码放在byte类型的数组中
        FileOutputStream f2 = new FileOutputStream("keykb1.dat");//指定文件输出流
        f2.write(kb);//保存密钥编码
        System.out.println("AES密钥完成字节码保存");
        return kb;
    }
    //AES加密，返回密文。
    public static byte[] EncryptionAES(String Plaintext) throws Exception {
        byte data[]=Plaintext.getBytes("UTF8");
        FileInputStream f = new FileInputStream("AESkey.dat");
        ObjectInputStream b = new ObjectInputStream(f);
        Key k = (Key) b.readObject();//从文件中获取密钥
        Cipher cp = Cipher.getInstance("AES");//创建密码器
        cp.init(Cipher.ENCRYPT_MODE, k);//初始化密码器，第一个参数指定密码器准备进行加密还是解密，第二个参数则传入加密或解密所使用的密钥。
        byte ctext[] = cp.doFinal(data);//执行Cipher对象的doFinal()方法，该方法的参数中传入待加密的明文，从而按照前面设置的算法与模式对明文加密。
        System.out.println("AES加密完成！");
        FileOutputStream f2 = new FileOutputStream("Ciphertext.txt");//将密文保存于Ciphertext.dat中。
        f2.write(ctext);//处理加密结果
        return ctext;
    }
    //AES解密，信息保存。
    public static String DecryptionAES(byte[]data) throws Exception{
        FileInputStream f = new FileInputStream("AESkey.dat");
        ObjectInputStream b = new ObjectInputStream(f);
        Key k = (Key) b.readObject();//从文件中获取密钥
        Cipher cp = Cipher.getInstance("AES");//创建密码器
        cp.init(Cipher.DECRYPT_MODE,k);
        byte ptext[]=cp.doFinal(data);
        String p=new String(ptext,"UTF8");
        //System.out.println(p);
        System.out.println("AES密钥解密完成！");
        return p;
    }

}

