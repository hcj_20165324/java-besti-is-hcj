


import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class Dome01 {
    public static void main(String[] args) {
        byte[] a = {12,-46,47,-66,87};
        String s = new String(a);
        String encoded = Base64.getEncoder().encodeToString(a);
        byte[] decoded = Base64.getDecoder().decode(encoded);
        for(int i=0;i<decoded.length;i++) {
            System.out.print(decoded[i]+" ");
        }
        System.out.println(" ");
        try {
            byte[] b = s.getBytes("UTF8");
            for(int i=0;i<b.length;i++) {
                System.out.print(b[i]+" ");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
