//输入学生个数，输入学生姓名和成绩，打印第一名，第二名的姓名和成绩。
import java.util.Scanner;
public class D3 
{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("输入学生个数:");
		int n=sc.nextInt();
		String name1="";//保存第一名
		int score1=0;
		String name2="";//保存第二名
		int score2=0;
		for(int i=1;i<=n;i++){
			System.out.println("请输入学生姓名:");
			String name=sc.next();
			System.out.println("请输入学生成绩:");
			int score=sc.nextInt();
			if(score>score1){
				//原来第一高分变为第二高分
				score2=score1;
				name2=name1;
				//刚刚输入的学生为最高分
				score1=score;
				name1=name;
			}
			else if(score>score2){
				score2=score;
				name2=name;
			}
		}
		if(n>=2){
		System.out.println("学生第一名:"+name1+"\t"+"成绩为:"+score1);
		System.out.println("学生第二名:"+name2+"\t"+"成绩为:"+score2);
		}
		else if(n==1)
			System.out.println("学生第一名:"+name1+"\t"+"成绩为:"+score1);
		else
			System.out.println("输入信息错误！");
		}
}
