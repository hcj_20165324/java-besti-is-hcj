/*设计数据结构，存储两个班级学生的信息，
包括学号、姓名、性别、数学成绩、物理成绩。
编程实现按学号升序序排列输出学生信息的功能。
（每班学生数不少于10人。）
*/
public class shiYan{
	String name;
	String number;
	String gender;
	int mathScore;
	int physicalScore;
	public void showInfo() {
		System.out.println("学生信息--------");
		System.out.println("学生学号为:"+number);
		System.out.println("学生姓名为:"+name);
		System.out.println("学生性别为:"+gender);
		System.out.println("学生数学成绩为:"+mathScore);
		System.out.println("学生物理成绩为:"+physicalScore);
	}
}