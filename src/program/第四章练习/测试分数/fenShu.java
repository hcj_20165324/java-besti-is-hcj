/*
//练习定义一个分数类,定义一个约分的方法
public class fenShu{
	int fenZi; //分子
	int fenMu; //分母
	//打印
	void show(){
		if(fenMu==0){
			System.out.println("分母不能为0");
		}
		yuefen();
		if(fenMu==1){
			System.out.println(fenZi);
		}
		else{
			System.out.println(fenZi+"/"+fenMu);
		}
	}
	//约分
	void yuefen (){
		int gcd=Gcd(fenMu,fenZi);//最大公约数
		fenZi=fenZi/gcd;
		fenMu=fenMu/gcd;
	}
	//求最大公约数
	int Gcd(int fenZi,int fenMu){
		for (int gcd=fenZi;gcd>=1;gcd--){
			if (fenZi/gcd==0&&fenMu/gcd==0){
				return gcd;
			}
		}
		return 1;
	}
	
}
总结：
1.成员变量可以在类中直接使用，定义方法时可以直接
void show(){
}
2.解决一个问题，尽量分而治之，逐步完善。
*/

public class fenShu {
	int fenzi;//分子
	int fenmu;//分母
	void show(){
		reduction();  //约分
		if(fenmu==1){
			System.out.println(fenzi);
		}
		else if (fenmu==0){
		System.out.println("分母不能为0！");
		}
		else{
			System.out.println(fenzi+"/"+fenmu);
		}
	}
	void reduction(){
		//先求最大公约数
		int gcd=getGcd(fenzi,fenmu);
		fenzi=fenzi/gcd;
		fenmu=fenmu/gcd;
	}
	int getGcd(int m,int n){
		for (int x=m;x>=1;x--){
			if (m%x==0&&n%x==0){
				return x;
			}
		}
		return 1;
	}
}