/*
ATM机类
属性：银行账户
操作：存款，取款，查询余额
*/
public class  Atm {
	//银行账户
	Account bankAccount;
	//从ATM上取款
	void withDraw(double money) {
	//实际上是从银行账户上取款，也就是减少银行账户上的余额
		bankAccount.withDraw(money);
	}
	void deposit (double money) {
		bankAccount.deposit(money);
	}
	void showInfo() {
		bankAccount.showInfo();
	}
}
