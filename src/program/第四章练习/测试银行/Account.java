/*创建银行账户类Account，包括账户ID，账户余额balance，创建日期dateCreate
取款方法withDraw,存款方法deposit。
*/
public class Account {
	String ID;
	double balance=0;
	String dateCreate;
	void withDraw(double a) {
		if(a>balance) {
			System.out.println("余额不足，zz");
			return ;
		}
		balance-=a;
	}
	void deposit(double b) {
		balance+=b;
	}
	void showInfo(){
		System.out.println("银行个人信息");
		System.out.println("账户ID:"+ID);
	    System.out.println("创建日期:"+dateCreate);
		System.out.println("账户余额:"+balance);
	}
}
