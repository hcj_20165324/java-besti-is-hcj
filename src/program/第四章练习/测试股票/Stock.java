/*
写一个股票类Stock,包括一个名为symbol的字符串代码表示股票代码；                                 
一个name字段表示股票名字；一个previousPrice的double类型字段表示前一天的股值；
一个currentPrice的字段表示当前的股票值；创建构造方法；
创建一个getChangePercent()的方法返回股票值变化的百分比；
*/
public class Stock{
	String symbol; // 股票代码
	String name;   // 股票名字
    double previousPrice;
	double currentPrice;
	public void getPreviousPrice(double a) {
		previousPrice=a;
	}
	public void getCurrentPrice(double b) {
		currentPrice=b;
	}
	public double getChangePercent() {
		return (currentPrice-previousPrice) /previousPrice;
	}
	public void showInfo() {
		System.out.println("股票信息-----------------");
		System.out.println("股票名字:"+name);
		System.out.println("股票代码:"+symbol);
		System.out.println("前一天股值:"+previousPrice);
		System.out.println("今天股值:"+currentPrice);
		System.out.println("涨停值:"+getChangePercent());
	}
}